﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeCacheUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Caching;
    using NUnit.Framework;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements unit tests for the <see cref="AttributeCache{TAttribute}"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    public class AttributeCacheUnitTests
    {
        /// <summary>
        /// The <see cref="ObjectCache"/> instance for test purposes.
        /// </summary>
        private MemoryCache objectCache;

        /// <summary>
        /// Sample test attributes.
        /// </summary>
        private ObjectAttribute[] sampleAttributes;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.objectCache = new MemoryCache("Test");

            this.sampleAttributes = new[]
            {
                new ObjectAttribute { Key = "Attr1", TextValue = "Value1" },
                new ObjectAttribute { Key = "Attr2", TextValue = "Value2" },
                new ObjectAttribute { Key = "Attr3", TextValue = "Value3" }
            };
        }

        /// <summary>
        /// Cleans up the test fixture after each test.
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            if (this.objectCache != null)
            {
                this.objectCache.Dispose();
            }
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeCache{TAttribute}"/> class throws an
        /// <see cref="ArgumentNullException"/> when the specified <see cref="ObjectCache"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateAttributeCacheWhenObjectCacheNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AttributeCache<ObjectAttribute>(null));

            Assert.That(exception.ParamName, Is.EqualTo("objectCache"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeCache{TAttribute}"/> class correctly initializes a
        /// new instance when the specified <see cref="ObjectCache"/> is not <c>null</c>.
        /// </summary>
        [Test]
        public void CreateAttributeCacheWhenObjectCacheNotNull()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);

            Assert.That(cache.ObjectCache, Is.SameAs(this.objectCache));
            Assert.That(cache.CachePolicy, Is.Not.Null);
            Assert.That(cache.MaximumSize, Is.EqualTo(AttributeCache<ObjectAttribute>.DefaultMaximumSize));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeCache{TAttribute}"/> class correctly initializes a
        /// new instance when the specified <see cref="ObjectCache"/> is not <c>null</c> and the specified
        /// <see cref="CacheItemPolicy"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateAttributeCacheWhenObjectCacheNotNullAndCachePolicyNull()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache, null);

            Assert.That(cache.ObjectCache, Is.SameAs(this.objectCache));
            Assert.That(cache.CachePolicy, Is.Not.Null);
            Assert.That(cache.MaximumSize, Is.EqualTo(AttributeCache<ObjectAttribute>.DefaultMaximumSize));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeCache{TAttribute}"/> class correctly initializes a
        /// new instance when the specified <see cref="ObjectCache"/> is not <c>null</c> and the specified
        /// <see cref="CacheItemPolicy"/> is not <c>null</c>.
        /// </summary>
        [Test]
        public void CreateAttributeCacheWhenObjectCacheNotNullAndCachePolicyNotNull()
        {
            var policy = new CacheItemPolicy();
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache, policy);

            Assert.That(cache.ObjectCache, Is.SameAs(this.objectCache));
            Assert.That(cache.CachePolicy, Is.EqualTo(policy));
            Assert.That(cache.MaximumSize, Is.EqualTo(AttributeCache<ObjectAttribute>.DefaultMaximumSize));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.AddOrGetAttributes"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified delegate is <c>null</c>.
        /// </summary>
        [Test]
        public void AddOrGetAttributesWhenFuncNull()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => cache.AddOrGetAttributes(Guid.NewGuid(), null));

            Assert.That(exception.ParamName, Is.EqualTo("func"));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.AddOrGetAttributes"/> method works correctly when the
        /// attributes of the specified object are present in the cache.
        /// </summary>
        [Test]
        public void AddOrGetAttributesWhenAttributesInCache()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);

            Guid objectId = Guid.NewGuid();
            this.objectCache.Add(objectId.ToString(), this.sampleAttributes, DateTimeOffset.MaxValue);

            IEnumerable<ObjectAttribute> result = cache.AddOrGetAttributes(objectId, () => null);

            Assert.That(result, Is.SameAs(this.sampleAttributes));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.AddOrGetAttributes"/> method works correctly when the
        /// attributes of the specified object are not present in the cache.
        /// </summary>
        [Test]
        public void AddOrGetAttributesWhenAttributesNotInCache()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);
            Guid objectId = Guid.NewGuid();

            IEnumerable<ObjectAttribute> result = cache.AddOrGetAttributes(objectId, () => this.sampleAttributes);

            Assert.That(result, Is.SameAs(this.sampleAttributes));
            Assert.That(this.objectCache[objectId.ToString()], Is.SameAs(this.sampleAttributes));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.AddOrGetAttributes"/> method works correctly when the
        /// attributes of the specified object are not present in the cache and the cache contains the maximum allowed
        /// number of entries.
        /// </summary>
        [Test]
        public void AddOrGetAttributesWhenAttributesNotInCacheAndCacheFull()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache, 10);

            for (int i = 0; i < 10; i++)
            {
                this.objectCache.Add(Guid.NewGuid().ToString(), this.sampleAttributes, cache.CachePolicy);
            }

            Guid objectId = Guid.NewGuid();
            IEnumerable<ObjectAttribute> result = cache.AddOrGetAttributes(objectId, () => this.sampleAttributes);

            Assert.That(result, Is.SameAs(this.sampleAttributes));

            long count = this.objectCache.GetCount();
            Assert.That(count, Is.LessThan(10));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.AddAttributes"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified attribute sequence is <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributesWhenAttributesNull()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => cache.AddAttributes(Guid.NewGuid(), null));

            Assert.That(exception.ParamName, Is.EqualTo("attributes"));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.AddAttributes"/> method works correctly when the
        /// attributes of the specified object are present in the cache.
        /// </summary>
        [Test]
        public void AddAttributesWhenAttributesInCache()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);

            Guid objectId = Guid.NewGuid();
            this.objectCache.Add(objectId.ToString(), this.sampleAttributes, DateTimeOffset.MaxValue);

            var attributes = new[]
            {
                new ObjectAttribute { Key = "NewAttr1", TextValue = "NewValue1" },
                new ObjectAttribute { Key = "NewAttr2", TextValue = "NewValue2" },
                new ObjectAttribute { Key = "NewAttr3", TextValue = "NewValue3" }
            };

            cache.AddAttributes(objectId, attributes);

            Assert.That(this.objectCache[objectId.ToString()], Is.SameAs(attributes));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.AddAttributes"/> method works correctly when the
        /// attributes of the specified object are not present in the cache.
        /// </summary>
        [Test]
        public void AddAttributesWhenAttributesNotInCache()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);
            Guid objectId = Guid.NewGuid();
            
            var attributes = new[]
            {
                new ObjectAttribute { Key = "NewAttr1", TextValue = "NewValue1" },
                new ObjectAttribute { Key = "NewAttr2", TextValue = "NewValue2" },
                new ObjectAttribute { Key = "NewAttr3", TextValue = "NewValue3" }
            };

            cache.AddAttributes(objectId, attributes);

            Assert.That(this.objectCache[objectId.ToString()], Is.SameAs(attributes));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.AddAttributes"/> method works correctly when the
        /// attributes of the specified object are not present in the cache and the cache contains the maximum allowed
        /// number of entries.
        /// </summary>
        [Test]
        public void AddAttributesWhenAttributesNotInCacheAndCacheFull()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache, 10);

            for (int i = 0; i < 10; i++)
            {
                this.objectCache.Add(Guid.NewGuid().ToString(), this.sampleAttributes, cache.CachePolicy);
            }

            Guid objectId = Guid.NewGuid();
            cache.AddAttributes(objectId, this.sampleAttributes);

            long count = this.objectCache.GetCount();
            Assert.That(count, Is.LessThan(10));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.RemoveAttributes"/> method works correctly when the
        /// attributes of the specified object are present in the cache.
        /// </summary>
        [Test]
        public void RemoveAttributesWhenAttributesInCache()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);

            Guid objectId = Guid.NewGuid();
            this.objectCache.Add(objectId.ToString(), this.sampleAttributes, DateTimeOffset.MaxValue);

            cache.RemoveAttributes(objectId);

            Assert.That(this.objectCache[objectId.ToString()], Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="AttributeCache{TAttribute}.RemoveAttributes"/> method works correctly when the
        /// attributes of the specified object are not present in the cache.
        /// </summary>
        [Test]
        public void RemoveAttributesWhenAttributesNotInCache()
        {
            var cache = new AttributeCache<ObjectAttribute>(this.objectCache);
            Guid objectId = Guid.NewGuid();

            cache.RemoveAttributes(objectId);

            Assert.That(this.objectCache[objectId.ToString()], Is.Null);
        }
    }
}