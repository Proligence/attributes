﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeManagerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Runtime.Caching;
    using Moq;
    using NUnit.Framework;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements unit tests for the <see cref="AttributeManager{TAttribute}"/> class.
    /// </summary>
    [TestFixture]
    [SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
    public class AttributeManagerUnitTests
    {
        /// <summary>
        /// The tested <see cref="AttributeManager{TAttribute}"/> instance.
        /// </summary>
        private AttributeManagerMock attributeManager;

        /// <summary>
        /// The mocked attribute store.
        /// </summary>
        private Mock<IAttributeStore> attributeStore;

        /// <summary>
        /// The attribute cache.
        /// </summary>
        private AttributeCache<ObjectAttribute> attributeCache;

        /// <summary>
        /// The underlying object cache.
        /// </summary>
        private MemoryCache objectCache;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.attributeStore = new Mock<IAttributeStore>();
            this.objectCache = new MemoryCache("Test");
            this.attributeCache = new AttributeCache<ObjectAttribute>(this.objectCache);
            this.attributeManager = new AttributeManagerMock(this.attributeStore.Object, this.attributeCache);
        }

        /// <summary>
        /// Cleans up the test fixture after each test.
        /// </summary>
        [TearDown]
        public void Teardown()
        {
            if (this.objectCache != null)
            {
                this.objectCache.Dispose();
            }
        }

        /// <summary>
        /// Tests if new instance of <see cref="AttributeManager{TAttribute}"/> are properly created.
        /// </summary>
        [Test]
        public void CreateAttributeManager()
        {
            var store = new Mock<IAttributeStore>();
            var cache = new Mock<IAttributeCache<ObjectAttribute>>(); 
            var manager = new AttributeManagerMock(store.Object, cache.Object);

            Assert.That(manager.Store, Is.SameAs(store.Object));
            Assert.That(manager.Cache, Is.SameAs(cache.Object));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeManager{TAttribute}"/> class throws a
        /// <see cref="ArgumentNullException"/> when the specified attribute store is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateAttributeManagerWhenStoreNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AttributeManagerMock(null, this.attributeCache));

            Assert.That(exception.ParamName, Is.EqualTo("store"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeManager{TAttribute}"/> class throws a
        /// <see cref="ArgumentNullException"/> when the specified attribute cache is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateAttributeManagerWhenCacheNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AttributeManagerMock(this.attributeStore.Object, null));

            Assert.That(exception.ParamName, Is.EqualTo("cache"));
        }

        /// <summary>
        /// Tests if the attributes of the object with the specified identifier are returned correctly from the
        /// underlying store when the object has no attributes.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenObjectHasNoAttributes()
        {
            Guid objectId = Guid.NewGuid();
            var attributes = new AttributeData[0];
            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            IEnumerable<ObjectAttribute> actualAttributes = this.attributeManager.GetObjectAttributes(objectId);

            Assert.That(actualAttributes, Is.Empty);
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if attributes with a text value are correctly returned from the underlying attribute store.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenTextValueSpecified()
        {
            Guid objectId = Guid.NewGuid();
            var attributeData = new AttributeData { Key = "Attr", TextValue = "Value" };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(new[] { attributeData });

            ObjectAttribute[] actualAttributes = this.attributeManager.GetObjectAttributes(objectId).ToArray();

            Assert.That(actualAttributes[0].Key, Is.EqualTo("Attr"));
            Assert.That(actualAttributes[0].TextValue, Is.EqualTo("Value"));
            
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if attributes with an XML value are correctly returned from the underlying attribute store.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenXmlValueSpecified()
        {
            Guid objectId = Guid.NewGuid();

            var attributeData = new AttributeData
            {
                Key = "Attr", 
                XmlValue = "<Root><Child>Test</Child></Root>"
            };

            this.attributeStore.Setup(x => x.GetAttributes(objectId))
                .Returns(new[] { attributeData });

            ObjectAttribute[] actualAttributes = this.attributeManager.GetObjectAttributes(objectId).ToArray();

            Assert.That(actualAttributes[0].Key, Is.EqualTo("Attr"));

            string normalizedXml = actualAttributes[0].XmlValue.CreateNavigator().OuterXml
                .Replace(" ", string.Empty)
                .Replace("\t", string.Empty)
                .Replace("\r", string.Empty)
                .Replace("\n", string.Empty);

            Assert.That(normalizedXml, Is.EqualTo("<Root><Child>Test</Child></Root>"));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if attributes with a GUID reference value are correctly returned from the underlying attribute store.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenGuidRefValueSpecified()
        {
            Guid objectId = Guid.NewGuid();
            Guid refValue = Guid.NewGuid();
            var attributeData = new AttributeData { Key = "Attr", GuidRefValue = refValue };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(new[] { attributeData });

            ObjectAttribute[] actualAttributes = this.attributeManager.GetObjectAttributes(objectId).ToArray();

            Assert.That(actualAttributes[0].Key, Is.EqualTo("Attr"));
            
            /* ReSharper disable PossibleInvalidOperationException */
            Assert.That(actualAttributes[0].GuidRefValue.Value, Is.EqualTo(refValue));
            /* ReSharper restore PossibleInvalidOperationException */

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the attributes of the object with the specified identifier are returned correctly from the
        /// underlying store when the object has attributes with unique names.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenObjectHasUniqueAttributes()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
            {
                new AttributeData { Key = "Attr1", TextValue = "Value1" },
                new AttributeData { Key = "Attr2", TextValue = "Value2" },
                new AttributeData { Key = "Attr3", TextValue = "Value3" }
            };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            ObjectAttribute[] actualAttributes = this.attributeManager.GetObjectAttributes(objectId).ToArray();

            Assert.That(actualAttributes.Length, Is.EqualTo(3));
            Assert.That(actualAttributes[0].Key, Is.EqualTo("Attr1"));
            Assert.That(actualAttributes[0].TextValue, Is.EqualTo("Value1"));
            Assert.That(actualAttributes[1].Key, Is.EqualTo("Attr2"));
            Assert.That(actualAttributes[1].TextValue, Is.EqualTo("Value2"));
            Assert.That(actualAttributes[2].Key, Is.EqualTo("Attr3"));
            Assert.That(actualAttributes[2].TextValue, Is.EqualTo("Value3"));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the attributes of the object with the specified identifier are returned correctly from the
        /// underlying store when the object has attributes with duplicate names.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenObjectHasDuplicateAttributes()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
            {
                new AttributeData { Key = "Attr", TextValue = "Value1" },
                new AttributeData { Key = "Attr", TextValue = "Value2" },
                new AttributeData { Key = "Attr", TextValue = "Value3" }
            };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            ObjectAttribute[] actualAttributes = this.attributeManager.GetObjectAttributes(objectId).ToArray();

            Assert.That(attributes.Length, Is.EqualTo(3));
            Assert.That(actualAttributes[0].Key, Is.EqualTo("Attr"));
            Assert.That(actualAttributes[0].TextValue, Is.EqualTo("Value1"));
            Assert.That(actualAttributes[1].Key, Is.EqualTo("Attr"));
            Assert.That(actualAttributes[1].TextValue, Is.EqualTo("Value2"));
            Assert.That(actualAttributes[2].Key, Is.EqualTo("Attr"));
            Assert.That(actualAttributes[2].TextValue, Is.EqualTo("Value3"));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>GetObjectAttributes</c> method properly uses the underlying attribute cache.
        /// </summary>
        [Test]
        public void GetObjectAttributesEnsureCacheIsUsed()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
            {
                new AttributeData { Key = "Attr", TextValue = "Value1" },
                new AttributeData { Key = "Attr", TextValue = "Value2" },
                new AttributeData { Key = "Attr", TextValue = "Value3" }
            };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            IEnumerable<ObjectAttribute> actualAttributes1 = this.attributeManager.GetObjectAttributes(objectId);

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Throws(
                new InvalidOperationException("Cache should be used instead of the store!"));

            IEnumerable<ObjectAttribute> actualAttributes2 = this.attributeManager.GetObjectAttributes(objectId);

            Assert.That(actualAttributes2, Is.SameAs(actualAttributes1));
        }

        /// <summary>
        /// Tests if the attributes of the object with the specified identifier are returned correctly from the
        /// underlying store when the object has no attributes.
        /// </summary>
        [Test]
        public void GetObjectAttributesByKeyWhenObjectHasNoAttributes()
        {
            Guid objectId = Guid.NewGuid();
            var attributes = new AttributeData[0];
            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            IEnumerable<ObjectAttribute> actualAttributes = 
                this.attributeManager.GetObjectAttributes(objectId, "Attr");

            Assert.That(actualAttributes, Is.Empty);
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the attributes with the specified key of the object with the specified identifier are returned
        /// correctly from the underlying store when the object has multiple attributes.
        /// </summary>
        [Test]
        public void GetObjectAttributesByKeyWhenObjectHasAttributes()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
            {
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value1" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value2" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value3" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "OtherAttr", TextValue = "Value3" }
            };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            IEnumerable<ObjectAttribute> actualAttributes = 
                this.attributeManager.GetObjectAttributes(objectId, "Attr");

            Guid[] expectedIds = new[]
            {
                attributes[0].AttributeId, 
                attributes[1].AttributeId, 
                attributes[2].AttributeId
            };

            Assert.That(actualAttributes.Select(x => x.AttributeId).ToArray(), Is.EquivalentTo(expectedIds));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if attributes with attribute data members are returned correctly from the underlying attribute store.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenObjectHasDefaultAttributeMember()
        {
            var attrCache = new AttributeCache<AttributeWithMembers>(this.objectCache);
            var attrManager = new AttributeManagerMock<AttributeWithMembers>(this.attributeStore.Object, attrCache);

            Guid objectId = Guid.NewGuid();

            var attributeData = new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value1" };
            attributeData.DataMembers.Add("StringProperty", "Test");

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(new[] { attributeData });

            IEnumerable<AttributeWithMembers> actualAttributes = attrManager.GetObjectAttributes(objectId, "Attr");

            Assert.That(actualAttributes.Single().StringProperty, Is.EqualTo("Test"));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if attributes with attribute data members with custom names are returned correctly from the
        /// underlying attribute store.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenObjectHasAttributeMemberWithCustomName()
        {
            var attrCache = new AttributeCache<AttributeWithMembers>(this.objectCache);
            var attrManager = new AttributeManagerMock<AttributeWithMembers>(this.attributeStore.Object, attrCache);

            Guid objectId = Guid.NewGuid();

            var attributeData = new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value1" };
            attributeData.DataMembers.Add("Int", 7);

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(new[] { attributeData });

            IEnumerable<AttributeWithMembers> actualAttributes = attrManager.GetObjectAttributes(objectId, "Attr");

            Assert.That(actualAttributes.Single().IntegerProperty, Is.EqualTo(7));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if attributes with read-only attribute data members are returned correctly from the underlying
        /// attribute store.
        /// </summary>
        [Test]
        public void GetObjectAttributesWhenObjectHasReadOnlyAttributeMembers()
        {
            var attrCache = new AttributeCache<AttributeWithMembers>(this.objectCache);
            var attrManager = new AttributeManagerMock<AttributeWithMembers>(this.attributeStore.Object, attrCache);

            Guid objectId = Guid.NewGuid();

            var attributeData = new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value1" };
            attributeData.DataMembers.Add("BooleanProperty", true);

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(new[] { attributeData });

            IEnumerable<AttributeWithMembers> actualAttributes = attrManager.GetObjectAttributes(objectId, "Attr");

            Assert.That(actualAttributes.Single().BooleanProperty, Is.EqualTo(true));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>GetObjectAttributes</c> method properly uses the underlying attribute cache when an
        /// attribute key (name) is specified.
        /// </summary>
        [Test]
        public void GetAttributesByKeyEnsureCacheIsUsed()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
            {
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value1" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value2" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value3" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "OtherAttr", TextValue = "Value3" }
            };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);
            
            this.attributeManager.GetObjectAttributes(objectId, "Attr");

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Throws(
                new InvalidOperationException("The cache should be used instead of the store!"));

            ObjectAttribute[] actualAttributes = 
                this.attributeManager.GetObjectAttributes(objectId, "Attr").ToArray();

            Assert.That(actualAttributes.Length, Is.EqualTo(3));
            Assert.That(actualAttributes[0].AttributeId, Is.EqualTo(attributes[0].AttributeId));
            Assert.That(actualAttributes[1].AttributeId, Is.EqualTo(attributes[1].AttributeId));
            Assert.That(actualAttributes[2].AttributeId, Is.EqualTo(attributes[2].AttributeId));
        }

        /// <summary>
        /// Tests if the <c>SelectAttributes</c> method throws a <see cref="ArgumentNullException"/> when the
        /// specified selection delegate is <c>null</c>.
        /// </summary>
        [Test]
        public void SelectObjectAttributesWhenSelectorNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.attributeManager.SelectObjectAttributes(Guid.NewGuid(), null));

            Assert.That(exception.ParamName, Is.EqualTo("selector"));
        }

        /// <summary>
        /// Tests if the attributes of the object with the specified identifier are correctly selected from the
        /// underlying store when the specified delegate does not select any attributes.
        /// </summary>
        [Test]
        public void SelectObjectAttributesWhenNoAttributesReturned()
        {
            Guid objectId = Guid.NewGuid();
            var attributes = new AttributeData[0];
            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            IEnumerable<ObjectAttribute> actualAttributes = 
                this.attributeManager.SelectObjectAttributes(objectId, attr => true);

            Assert.That(actualAttributes, Is.Empty);
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the attributes of the object with the specified identifier are correctly selected from the
        /// underlying store when the specified delegate selects attributes with unique names.
        /// </summary>
        [Test]
        public void SelectObjectAttributesWhenUniqueAttributesReturned()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
            {
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr1", TextValue = "Value1" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr2", TextValue = "Value2" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr3", TextValue = "x1" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr4", TextValue = "Value3" },
                new AttributeData { AttributeId = Guid.NewGuid(), Key = "OtherAttr", TextValue = "x2" }
            };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            IEnumerable<ObjectAttribute> actualAttributes = this.attributeManager.SelectObjectAttributes(
                objectId, 
                attr => attr.TextValue.StartsWith("V", StringComparison.Ordinal));

            Guid[] expectedIds = new[]
            {
                attributes[0].AttributeId, 
                attributes[1].AttributeId, 
                attributes[3].AttributeId
            };

            Assert.That(actualAttributes.Select(x => x.AttributeId).ToArray(), Is.EquivalentTo(expectedIds));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the attributes of the object with the specified identifier are correctly selected from the
        /// underlying store when the specified delegate selects attributes with duplicate names.
        /// </summary>
        [Test]
        public void SelectObjectAttributesWhenDuplicateAttributesReturned()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
                {
                    new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value1" },
                    new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value2" },
                    new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "x1" },
                    new AttributeData { AttributeId = Guid.NewGuid(), Key = "Attr", TextValue = "Value3" },
                    new AttributeData { AttributeId = Guid.NewGuid(), Key = "OtherAttr", TextValue = "x2" }
                };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            IEnumerable<ObjectAttribute> actualAttributes = this.attributeManager.SelectObjectAttributes(
                objectId,
                attr => attr.TextValue.StartsWith("V", StringComparison.Ordinal));

            Guid[] expectedIds = new[]
            {
                attributes[0].AttributeId,
                attributes[1].AttributeId,
                attributes[3].AttributeId
            };

            Assert.That(actualAttributes.Select(x => x.AttributeId).ToArray(), Is.EquivalentTo(expectedIds));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>SelectObjectAttributes</c> method properly uses the underlying attribute cache.
        /// </summary>
        [Test]
        public void SelectObjectAttributesEnsureCacheIsUsed()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
            {
                new AttributeData { Key = "Attr1", TextValue = "Value1" },
                new AttributeData { Key = "Attr2", TextValue = "Value2" },
                new AttributeData { Key = "Attr3", TextValue = "x1" },
                new AttributeData { Key = "Attr4", TextValue = "Value3" },
                new AttributeData { Key = "OtherAttr", TextValue = "x2" }
            };

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Returns(attributes);

            this.attributeManager.SelectObjectAttributes(
                objectId,
                attr => attr.TextValue.StartsWith("V", StringComparison.Ordinal));

            this.attributeStore.Setup(x => x.GetAttributes(objectId)).Throws(
                new InvalidOperationException("The cache should be used instead of the store!"));

            ObjectAttribute[] actualAttributes = this.attributeManager.SelectObjectAttributes(
                objectId,
                attr => attr.TextValue.StartsWith("V", StringComparison.Ordinal))
                .ToArray();

            Assert.That(actualAttributes.Length, Is.EqualTo(3));
            Assert.That(actualAttributes[0].AttributeId, Is.EqualTo(attributes[0].AttributeId));
            Assert.That(actualAttributes[1].AttributeId, Is.EqualTo(attributes[1].AttributeId));
            Assert.That(actualAttributes[2].AttributeId, Is.EqualTo(attributes[3].AttributeId));
        }

        /// <summary>
        /// Tests if attributes with the specified identifier are correctly returned from the underlying store when
        /// the attribute is present in the store.
        /// </summary>
        [Test]
        public void GetAttributeWhenAttributeExists()
        {
            Guid attributeId = Guid.NewGuid();
            
            var attribute = new AttributeData
            {
                AttributeId = attributeId, 
                Key = "Attr1", 
                TextValue = "Value1"
            };

            this.attributeStore.Setup(x => x.GetAttribute(attributeId)).Returns(attribute);

            ObjectAttribute actualAttribute = this.attributeManager.GetAttribute(attributeId);

            Assert.That(actualAttribute.AttributeId, Is.EqualTo(attributeId));
            Assert.That(actualAttribute.Key, Is.EqualTo("Attr1"));
            Assert.That(actualAttribute.TextValue, Is.EqualTo("Value1"));
            
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if attributes with the specified identifier are correctly returned from the underlying store when
        /// the attribute are not present in the store.
        /// </summary>
        [Test]
        public void GetAttributeWhenAttributeDoesNotExist()
        {
            Guid attributeId = Guid.NewGuid();
            this.attributeStore.Setup(x => x.GetAttribute(attributeId)).Returns((AttributeData)null);

            ObjectAttribute actualAttribute = this.attributeManager.GetAttribute(attributeId);

            Assert.That(actualAttribute, Is.Null);
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>SaveAttributes</c> method throws a <see cref="ArgumentNullException"/> when the specified
        /// attributes sequence is <c>null</c>.
        /// </summary>
        [Test]
        public void SaveAttributesWhenAttributesNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.attributeManager.SaveAttributes(null, false));

            Assert.That(exception.ParamName, Is.EqualTo("attributes"));
        }

        /// <summary>
        /// Tests if the <c>SaveAttributes</c> method throws a <see cref="ArgumentException"/> when the specified
        /// attributes sequence contains a <c>null</c> value.
        /// </summary>
        [Test]
        public void SaveAttributesWhenAttributesContainsNull()
        {
            var attributes = new[]
            {
                new ObjectAttribute { Key = "Attr1", TextValue = "Value1" },
                null,
                new ObjectAttribute { Key = "Attr3", TextValue = "Value3" }
            };

            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => this.attributeManager.SaveAttributes(attributes, false));

            Assert.That(exception.ParamName, Is.EqualTo("attributes"));
            Assert.That(exception.Message, Is.StringContaining("At least one of the specified attributes is null."));
        }

        /// <summary>
        /// Tests if the <c>SaveAttributes</c> method throws a <see cref="ArgumentException"/> when the specified
        /// attributes sequence contains an attribute with a key equal to <c>null</c> or <see cref="string.Empty"/>.
        /// </summary>
        /// <param name="key">The key value to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void SaveAttributesWhenAttributesContainsAttributeWithoutKey(string key)
        {
            var attributes = new[]
            {
                new ObjectAttribute { Key = "Attr1", TextValue = "Value1" },
                new ObjectAttribute { Key = key, TextValue = "Value2" },
                new ObjectAttribute { Key = "Attr3", TextValue = "Value3" }
            };

            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => this.attributeManager.SaveAttributes(attributes, false));

            Assert.That(exception.ParamName, Is.EqualTo("attributes"));
            Assert.That(
                exception.Message,
                Is.StringContaining("At least one of the specified attributes has an invalid key (name)."));
        }

        /// <summary>
        /// Tests if attributes with attribute data members are correctly saved to the underlying store.
        /// </summary>
        [Test]
        public void SaveAttributeWhenObjectHasDefaultAttributeMember()
        {
            var attrCache = new AttributeCache<AttributeWithMembers>(this.objectCache);
            var attrManager = new AttributeManagerMock<AttributeWithMembers>(this.attributeStore.Object, attrCache);

            var attribute = new AttributeWithMembers { Key = "Attr", TextValue = "Value" };
            attribute.StringProperty = "Test";

            IEnumerable<AttributeData> savedAttributes = null;
            this.attributeStore.Setup(x => x.SaveAttributes(It.IsAny<IEnumerable<AttributeData>>(), It.IsAny<bool>()))
                .Callback<IEnumerable<AttributeData>, bool>((attributeData, b) => savedAttributes = attributeData);

            attrManager.SaveAttributes(new[] { attribute }, false);

            Assert.That(savedAttributes.Single().DataMembers["StringProperty"], Is.EqualTo("Test"));
        }

        /// <summary>
        /// Tests if attributes with attribute data members with custom names are correctly saved to the underlying
        /// store.
        /// </summary>
        [Test]
        public void SaveAttributeWhenObjectHasAttributeMemberWithCustomName()
        {
            var attrCache = new AttributeCache<AttributeWithMembers>(this.objectCache);
            var attrManager = new AttributeManagerMock<AttributeWithMembers>(this.attributeStore.Object, attrCache);

            var attribute = new AttributeWithMembers { Key = "Attr", TextValue = "Value" };
            attribute.IntegerProperty = 7;

            IEnumerable<AttributeData> savedAttributes = null;
            this.attributeStore.Setup(x => x.SaveAttributes(It.IsAny<IEnumerable<AttributeData>>(), It.IsAny<bool>()))
                .Callback<IEnumerable<AttributeData>, bool>((attributeData, b) => savedAttributes = attributeData);

            attrManager.SaveAttributes(new[] { attribute }, false);

            Assert.That(savedAttributes.Single().DataMembers["Int"], Is.EqualTo(7));
        }

        /// <summary>
        /// Tests if attributes with read-only attribute data members are correctly saved to the underlying store.
        /// </summary>
        [Test]
        public void SaveAttributeWhenObjectHasReadOnlyAttributeMember()
        {
            var attrCache = new AttributeCache<AttributeWithMembers>(this.objectCache);
            var attrManager = new AttributeManagerMock<AttributeWithMembers>(this.attributeStore.Object, attrCache);

            var attribute = new AttributeWithMembers { Key = "Attr", TextValue = "Value" };

            IEnumerable<AttributeData> savedAttributes = null;
            this.attributeStore.Setup(x => x.SaveAttributes(It.IsAny<IEnumerable<AttributeData>>(), It.IsAny<bool>()))
                .Callback<IEnumerable<AttributeData>, bool>((attributeData, b) => savedAttributes = attributeData);

            attrManager.SaveAttributes(new[] { attribute }, false);

            Assert.That(savedAttributes.Single().DataMembers.ContainsKey("BooleanProperty"), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManager{TAttribute}.RemoveAttributes"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified sequence of attributes is <c>null</c>.
        /// </summary>
        [Test]
        public void RemoveAttributesWhenAttributesNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => this.attributeManager.RemoveAttributes(null));

            Assert.That(exception.ParamName, Is.EqualTo("attributeIds"));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManager{TAttribute}.RemoveAttributes"/> method works correctly when
        /// a valid sequence of attribute identifiers is specified.
        /// </summary>
        [Test]
        public void RemoveAttributesWhenValidArgs()
        {
            var attributeIds = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
            this.attributeStore.Setup(x => x.RemoveAttributes(attributeIds));

            this.attributeManager.RemoveAttributes(attributeIds);

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManager{TAttribute}.RemoveAttributes"/> method correctly removes
        /// the attributes from the cache.
        /// </summary>
        [Test]
        public void RemoveAttributesEnsureCacheUpdated()
        {
            Guid objectId = Guid.NewGuid();

            var attributes = new[]
            {
                new AttributeData
                { 
                    AttributeId = Guid.NewGuid(), 
                    ObjectId = objectId,
                    Key = "MyKey1", 
                    TextValue = "MyValue1" 
                },
                new AttributeData
                { 
                    AttributeId = Guid.NewGuid(), 
                    ObjectId = objectId,
                    Key = "MyKey2", 
                    TextValue = "MyValue2" 
                }
            };

            this.attributeStore.Setup(x => x.GetAttribute(attributes[0].AttributeId)).Returns(attributes[0]);

            ObjectAttribute[] objectAttributes = new[]
            {
                new ObjectAttribute
                {
                    AttributeId = Guid.NewGuid(),
                    ObjectId = objectId,
                    Key = "MyKey1",
                    TextValue = "MyValue1"
                }, new ObjectAttribute
                {
                    AttributeId = Guid.NewGuid(),
                    ObjectId = objectId,
                    Key = "MyKey2",
                    TextValue = "MyValue2"
                }
            };

            this.attributeCache.AddAttributes(
                objectId,
                objectAttributes);

            this.attributeManager.RemoveAttributes(new[] { attributes[0].AttributeId });

            Assert.That(this.attributeCache.ObjectCache.Contains(objectId.ToString()), Is.False);
            
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>SaveAttributes</c> method properly saves the specified attributes when the specified
        /// attributes are valid.
        /// </summary>
        [Test]
        public void SaveAttributesWhenValidArgs()
        {
            var attributes = new[]
            {
                new ObjectAttribute { AttributeId = Guid.NewGuid(), Key = "Attr1", TextValue = "Value1" },
                new ObjectAttribute { AttributeId = Guid.NewGuid(), Key = "Attr2", TextValue = "Value2" },
                new ObjectAttribute { AttributeId = Guid.NewGuid(), Key = "Attr3", TextValue = "Value3" }
            };

            AttributeData[] savedAttributesData = null;
            this.attributeStore.Setup(x => x.SaveAttributes(It.IsAny<IEnumerable<AttributeData>>(), It.IsAny<bool>()))
                .Callback<IEnumerable<AttributeData>, bool>((x, b) => savedAttributesData = x.ToArray());

            this.attributeManager.SaveAttributes(attributes, false);

            Assert.That(savedAttributesData.Length, Is.EqualTo(3));
            Assert.That(savedAttributesData[0].AttributeId, Is.EqualTo(attributes[0].AttributeId));
            Assert.That(savedAttributesData[0].Key, Is.EqualTo("Attr1"));
            Assert.That(savedAttributesData[0].TextValue, Is.EqualTo("Value1"));
            Assert.That(savedAttributesData[1].AttributeId, Is.EqualTo(attributes[1].AttributeId));
            Assert.That(savedAttributesData[1].Key, Is.EqualTo("Attr2"));
            Assert.That(savedAttributesData[1].TextValue, Is.EqualTo("Value2"));
            Assert.That(savedAttributesData[2].AttributeId, Is.EqualTo(attributes[2].AttributeId));
            Assert.That(savedAttributesData[2].Key, Is.EqualTo("Attr3"));
            Assert.That(savedAttributesData[2].TextValue, Is.EqualTo("Value3"));

            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <c>SaveAttributes</c> method properly updates the underlying attribute cache.
        /// </summary>
        [Test]
        public void SaveAttributesEnsureCacheUpdated()
        {
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();

            var attributes = new[]
            {
                new ObjectAttribute { ObjectId = objectId1, Key = "Attr1", TextValue = "Value1" },
                new ObjectAttribute { ObjectId = objectId1, Key = "Attr2", TextValue = "Value2" },
                new ObjectAttribute { ObjectId = objectId2, Key = "Attr3", TextValue = "Value3" }
            };

            this.attributeStore.Setup(
                x => x.SaveAttributes(It.IsAny<IEnumerable<AttributeData>>(), It.IsAny<bool>()));

            this.attributeCache.AddAttributes(objectId1, new ObjectAttribute[0]);
            this.attributeCache.AddAttributes(objectId2, new ObjectAttribute[0]);
            
            this.attributeManager.SaveAttributes(attributes, false);

            Assert.That(this.attributeCache.ObjectCache.Contains(objectId1.ToString()), Is.False);
            Assert.That(this.attributeCache.ObjectCache.Contains(objectId2.ToString()), Is.False);
            
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManager{TAttribute}.GetObjects"/> method works correctly.
        /// </summary>
        [Test]
        public void GetObjectsWhenValidArgs()
        {
            Guid refGuid = Guid.NewGuid();
            var expectedResult = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
            this.attributeStore.Setup(x => x.GetObjects("MyKey", "MyTextValue", refGuid)).Returns(expectedResult);

            IEnumerable<Guid> actualResult = this.attributeManager.GetObjects("MyKey", "MyTextValue", refGuid);

            Assert.That(actualResult, Is.SameAs(expectedResult));
            this.attributeStore.VerifyAll();
        }

        [Test]
        public void GetObjectsBySetWhenOperationSupportedByStore()
        {
            this.attributeStore = new Mock<IAttributeStore>();

            Guid refGuid = Guid.NewGuid();
            var textValues = new[] { "MyTextValue" };
            var expectedResult = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
            
            this.attributeStore.As<ISetQueryableStore>()
                .Setup(x => x.GetObjectsBySet("MyKey", textValues, refGuid))
                .Returns(expectedResult);

            this.attributeManager = new AttributeManagerMock(this.attributeStore.Object, this.attributeCache);

            IEnumerable<Guid> actualResult = this.attributeManager.GetObjectsBySet("MyKey", textValues, refGuid);

            Assert.That(actualResult, Is.SameAs(expectedResult));
            this.attributeStore.VerifyAll();
        }

        [Test]
        public void GetObjectsBySetWhenOperationNotSupportedByStore()
        {
            Guid refGuid = Guid.NewGuid();
            var textValues = new[] { "MyTextValue1", "MyTextValue2", "MyTextValue3" };
            var expectedResult = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };

            this.attributeStore.Setup(x => x.GetObjects("MyKey", "MyTextValue1", refGuid))
                .Returns(new[] { expectedResult[0] });

            this.attributeStore.Setup(x => x.GetObjects("MyKey", "MyTextValue2", refGuid))
                .Returns(new[] { expectedResult[1], expectedResult[0] });

            this.attributeStore.Setup(x => x.GetObjects("MyKey", "MyTextValue3", refGuid))
                .Returns(new[] { expectedResult[2] });
            
            IEnumerable<Guid> actualResult = this.attributeManager.GetObjectsBySet("MyKey", textValues, refGuid);

            Assert.That(actualResult, Is.EquivalentTo(expectedResult));
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManager{TAttribute}.GetReferencedObjects"/> method works correctly.
        /// </summary>
        [Test]
        public void GetReferencedObjectsWhenValidArgs()
        {
            Guid objectId = Guid.NewGuid();
            var expectedResult = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
            this.attributeStore.Setup(x => x.GetReferencedObjects("MyKey", "MyTextValue", objectId))
                .Returns(expectedResult);

            IEnumerable<Guid> actualResult = 
                this.attributeManager.GetReferencedObjects("MyKey", "MyTextValue", objectId);

            Assert.That(actualResult, Is.SameAs(expectedResult));
            this.attributeStore.VerifyAll();
        }

        [Test]
        public void GetReferencedObjectsBySetWhenOperationSupportedByStore()
        {
            this.attributeStore = new Mock<IAttributeStore>();

            Guid objectId = Guid.NewGuid();
            var textValues = new[] { "MyTextValue" };
            var expectedResult = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
            
            this.attributeStore.As<ISetQueryableStore>()
                .Setup(x => x.GetReferencedObjectsBySet("MyKey", textValues, objectId))
                .Returns(expectedResult);

            this.attributeManager = new AttributeManagerMock(this.attributeStore.Object, this.attributeCache);

            IEnumerable<Guid> actualResult = this.attributeManager.GetReferencedObjectsBySet(
                "MyKey",
                textValues,
                objectId);

            Assert.That(actualResult, Is.SameAs(expectedResult));
            this.attributeStore.VerifyAll();
        }

        [Test]
        public void GetReferencedObjectsBySetWhenOperationNotSupportedByStore()
        {
            Guid objectId = Guid.NewGuid();
            var textValues = new[] { "MyTextValue1", "MyTextValue2", "MyTextValue3" };
            var expectedResult = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };

            this.attributeStore.Setup(x => x.GetReferencedObjects("MyKey", "MyTextValue1", objectId))
                .Returns(new[] { expectedResult[0] });

            this.attributeStore.Setup(x => x.GetReferencedObjects("MyKey", "MyTextValue2", objectId))
                .Returns(new[] { expectedResult[1], expectedResult[0] });

            this.attributeStore.Setup(x => x.GetReferencedObjects("MyKey", "MyTextValue3", objectId))
                .Returns(new[] { expectedResult[2] });

            IEnumerable<Guid> actualResult = this.attributeManager.GetReferencedObjectsBySet(
                "MyKey",
                textValues,
                objectId);

            Assert.That(actualResult, Is.EquivalentTo(expectedResult));
            this.attributeStore.VerifyAll();
        }

        /// <summary>
        /// Attribute class with member properties.
        /// </summary>
        internal class AttributeWithMembers : ObjectAttribute
        {
            /// <summary>
            /// Gets or sets the string attribute member.
            /// </summary>
            [AttributeMember]
            public string StringProperty { get; set; }

            /// <summary>
            /// Gets or sets the integer attribute member.
            /// </summary>
            [AttributeMember(Name = "Int")]
            public int IntegerProperty { get; set; }

            /* ReSharper disable UnusedAutoPropertyAccessor.Local */

            /// <summary>
            /// Gets a value indicating whether the boolean attribute member is set.
            /// </summary>
            [AttributeMember(IsReadOnly = true)]
            public bool BooleanProperty { get; private set; }

            /* ReSharper restore UnusedAutoPropertyAccessor.Local */

            /// <summary>
            /// Gets or sets the unmapped (non-member) property.
            /// </summary>
            public string UnmappedProperty { get; set; }
        }
        
        /// <summary>
        /// Implements a subclass of the <see cref="AttributeManager"/> class for test purposes.
        /// </summary>
        /// <typeparam name="TAttribute">The type which represents the managed attributes.</typeparam>
        private class AttributeManagerMock<TAttribute> : AttributeManager<TAttribute>
            where TAttribute : ObjectAttribute, new()
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AttributeManagerMock{TAttribute}"/> class.
            /// </summary>
            /// <param name="store">The underlying attribute store.</param>
            /// <param name="cache">The attribute cache instance.</param>
            public AttributeManagerMock(
                IAttributeStore store,
                IAttributeCache<TAttribute> cache)
                : base(store, cache, new ReflectionAttributeMemberManager())
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="AttributeManagerMock{TAttribute}"/> class.
            /// </summary>
            /// <param name="store">The underlying attribute store.</param>
            /// <param name="cache">The attribute cache instance.</param>
            /// <param name="memberManager">The attribute member manager instance.</param>
            protected AttributeManagerMock(
                IAttributeStore store,
                IAttributeCache<TAttribute> cache,
                IAttributeMemberManager memberManager)
                : base(store, cache, memberManager)
            {
            }

            /// <summary>
            /// Gets the underlying attribute store.
            /// </summary>
            public new IAttributeStore Store
            {
                get { return base.Store; }
            }

            /// <summary>
            /// Gets the attribute cache used by the attribute manager.
            /// </summary>
            public new IAttributeCache<TAttribute> Cache
            {
                get { return base.Cache; }
            }
        }

        /// <summary>
        /// Implements a subclass of the <see cref="AttributeManager" /> class for test purposes.
        /// </summary>
        private class AttributeManagerMock : AttributeManagerMock<ObjectAttribute>
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AttributeManagerMock" /> class.
            /// </summary>
            /// <param name="store">The underlying attribute store.</param>
            /// <param name="cache">The attribute cache instance.</param>
            public AttributeManagerMock(
                IAttributeStore store,
                IAttributeCache<ObjectAttribute> cache)
                : base(store, cache, new ReflectionAttributeMemberManager())
            {
            }
        }
    }
}