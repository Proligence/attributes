﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributesCoreModuleUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core.UnitTests
{
    using Autofac;
    using NUnit.Framework;
    
    /// <summary>
    /// Implements unit tests for the <see cref="AttributesCoreModule"/> class.
    /// </summary>
    [TestFixture]
    public class AttributesCoreModuleUnitTests
    {
        /// <summary>
        /// The tested dependency injection container.
        /// </summary>
        private IContainer container;

        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new AttributesCoreModule());
            
            this.container = builder.Build();
        }

        /// <summary>
        /// Tests if the <see cref="IAttributeMemberManager"/> can be resolved from the container.
        /// </summary>
        [Test]
        public void ResolveAttributeMemberManager()
        {
            Assert.That(this.container.Resolve<IAttributeMemberManager>(), Is.Not.Null);
        }
    }
}