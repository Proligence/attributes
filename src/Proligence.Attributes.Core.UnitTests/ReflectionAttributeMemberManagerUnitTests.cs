﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionAttributeMemberManagerUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Moq;
    using Moq.Protected;
    using NUnit.Framework;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements unit tests for the <see cref="ReflectionAttributeMemberManager"/> class.
    /// </summary>
    [TestFixture]
    public class ReflectionAttributeMemberManagerUnitTests
    {
        /// <summary>
        /// Sets up the test fixture before each test.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            // Clear cached attribute member mappings
            BindingFlags bindingFlags = BindingFlags.Static | BindingFlags.NonPublic;
            FieldInfo fieldInfo = typeof(ReflectionAttributeMemberManager).GetField("Mappings", bindingFlags);

            /* ReSharper disable PossibleNullReferenceException */
            fieldInfo.SetValue(null, new Dictionary<Type, AttributeMemberMapping[]>());
            /* ReSharper restore PossibleNullReferenceException */
        }

        /// <summary>
        /// Tests if the <see cref="ReflectionAttributeMemberManager.GetMembersForAttributeType"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified type is <c>null</c>.
        /// </summary>
        [Test]
        public void GetMembersForAttributeTypeWhenTypeNull()
        {
            var manager = new ReflectionAttributeMemberManager();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => manager.GetMembersForAttributeType(null));

            Assert.That(exception.ParamName, Is.EqualTo("attributeType"));
        }

        /// <summary>
        /// Tests if the <see cref="ReflectionAttributeMemberManager.GetMembersForAttributeType"/> method works
        /// correctly when the specified type has no attribute member properties.
        /// </summary>
        [Test]
        public void GetMembersForAttributeTypeWhenTypeHasNoAttributeMembers()
        {
            var manager = new ReflectionAttributeMemberManager();

            IEnumerable<AttributeMemberMapping> mappings = 
                manager.GetMembersForAttributeType(typeof(AttributeWithoutMembers));

            Assert.That(mappings, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="ReflectionAttributeMemberManager.GetMembersForAttributeType"/> method works
        /// correctly when the specified type has a property marked with the <see cref="AttributeMemberAttribute"/>
        /// with all parameters set to default values.
        /// </summary>
        [Test]
        public void GetMembersForAttributeTypeWhenTypeHasDefaultAttributeMember()
        {
            var manager = new ReflectionAttributeMemberManager();

            IEnumerable<AttributeMemberMapping> mappings =
                manager.GetMembersForAttributeType(typeof(AttributeWithMembers));

            PropertyInfo expectedPropertyInfo = typeof(AttributeWithMembers).GetProperty("StringProperty");
            AttributeMemberMapping mapping = mappings.Single(x => x.Property.Equals(expectedPropertyInfo));

            Assert.That(mapping.MemberName, Is.EqualTo("StringProperty"));
            Assert.That(mapping.IsReadOnly, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ReflectionAttributeMemberManager.GetMembersForAttributeType"/> method works
        /// correctly when the specified type has a property marked with the <see cref="AttributeMemberAttribute"/>
        /// with the <see cref="AttributeMemberAttribute.Name"/> property set to a custom name.
        /// </summary>
        [Test]
        public void GetMembersForAttributeTypeWhenTypeHasAttributeMemberWithCustomName()
        {
            var manager = new ReflectionAttributeMemberManager();

            IEnumerable<AttributeMemberMapping> mappings =
                manager.GetMembersForAttributeType(typeof(AttributeWithMembers));

            PropertyInfo expectedPropertyInfo = typeof(AttributeWithMembers).GetProperty("IntegerProperty");
            AttributeMemberMapping mapping = mappings.Single(x => x.Property.Equals(expectedPropertyInfo));

            Assert.That(mapping.MemberName, Is.EqualTo("Int"));
            Assert.That(mapping.IsReadOnly, Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ReflectionAttributeMemberManager.GetMembersForAttributeType"/> method works
        /// correctly when the specified type has a property marked with the <see cref="AttributeMemberAttribute"/>
        /// with the <see cref="AttributeMemberAttribute.IsReadOnly"/> property set to <c>true</c>.
        /// </summary>
        [Test]
        public void GetMembersForAttributeTypeWhenTypeHasReadOnlyAttributeMember()
        {
            var manager = new ReflectionAttributeMemberManager();

            IEnumerable<AttributeMemberMapping> mappings =
                manager.GetMembersForAttributeType(typeof(AttributeWithMembers));

            PropertyInfo expectedPropertyInfo = typeof(AttributeWithMembers).GetProperty("BooleanProperty");
            AttributeMemberMapping mapping = mappings.Single(x => x.Property.Equals(expectedPropertyInfo));

            Assert.That(mapping.MemberName, Is.EqualTo("BooleanProperty"));
            Assert.That(mapping.IsReadOnly, Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ReflectionAttributeMemberManager.GetMembersForAttributeType"/> method works
        /// correctly when the specified type has properties which are not decorated with the
        /// <see cref="AttributeMemberAttribute"/>.
        /// </summary>
        [Test]
        public void GetMembersForAttributeTypeWhenTypeHasUnmappedProperties()
        {
            var manager = new ReflectionAttributeMemberManager();

            IEnumerable<AttributeMemberMapping> mappings =
                manager.GetMembersForAttributeType(typeof(AttributeWithMembers));

            Assert.That(mappings.Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Tests if the <see cref="ReflectionAttributeMemberManager.GetMembersForAttributeType"/> method works
        /// correctly when the specified type has attribute member properties inherited from the base class.
        /// </summary>
        [Test]
        public void GetMembersForAttributeTypeWhenTypeHasInheritedAttributeMembers()
        {
            var manager = new ReflectionAttributeMemberManager();

            AttributeMemberMapping[] mappings =
                manager.GetMembersForAttributeType(typeof(DerivedAttribute)).ToArray();

            Assert.That(mappings.Count(), Is.EqualTo(2));

            Assert.That(mappings.SingleOrDefault(x => x.MemberName == "Property1"), Is.Not.Null);
            Assert.That(mappings.SingleOrDefault(x => x.MemberName == "Property2"), Is.Not.Null);
        }

        /// <summary>
        /// Tests if the <see cref="ReflectionAttributeMemberManager.GetMembersForAttributeType"/> method correctly
        /// caches the results calculated for each attribute type.
        /// </summary>
        [Test]
        public void GetMembersForAttributeTypeEnsureResultsCached()
        {
            var managerMock = new Mock<ReflectionAttributeMemberManager> { CallBase = true };

            AttributeMemberMapping[] expectedMappings = new[]
            {
                new AttributeMemberMapping(
                    typeof(AttributeWithMembers).GetProperty("StringProperty"),
                    "StringProperty",
                    false),
                new AttributeMemberMapping(
                    typeof(AttributeWithMembers).GetProperty("IntegerProperty"),
                    "IntegerProperty",
                    false),
                new AttributeMemberMapping(
                    typeof(AttributeWithMembers).GetProperty("BooleanProperty"),
                    "BooleanProperty",
                    false)
            };

            managerMock.Protected().Setup<AttributeMemberMapping[]>("DiscoverMappings", typeof(AttributeWithMembers))
                .Returns(expectedMappings);

            IEnumerable<AttributeMemberMapping> mappings = 
                managerMock.Object.GetMembersForAttributeType(typeof(AttributeWithMembers));

            Assert.That(mappings, Is.SameAs(expectedMappings));

            managerMock.Protected().Setup<AttributeMemberMapping[]>("DiscoverMappings", typeof(AttributeWithMembers))
                .Throws(new InvalidOperationException("Cache should be used instead of DiscoverMappings method!"));

            IEnumerable<AttributeMemberMapping> mappings2 =
                managerMock.Object.GetMembersForAttributeType(typeof(AttributeWithMembers));

            Assert.That(mappings2, Is.SameAs(expectedMappings));
        }

        /// <summary>
        /// Attribute class without attribute member properties.
        /// </summary>
        internal class AttributeWithoutMembers : ObjectAttribute
        {
            /// <summary>
            /// Gets or sets the test property.
            /// </summary>
            public string Property1 { get; set; }
        }

        /// <summary>
        /// Attribute class with member properties.
        /// </summary>
        internal class AttributeWithMembers : ObjectAttribute
        {
            /// <summary>
            /// Gets or sets the string attribute member.
            /// </summary>
            [AttributeMember]
            public string StringProperty { get; set; }

            /// <summary>
            /// Gets or sets the integer attribute member.
            /// </summary>
            [AttributeMember(Name = "Int")]
            public string IntegerProperty { get; set; }
            
            /// <summary>
            /// Gets or sets the boolean attribute member
            /// </summary>
            [AttributeMember(IsReadOnly = true)]
            public string BooleanProperty { get; set; }

            /// <summary>
            /// Gets or sets the unmapped (non-member) property.
            /// </summary>
            public string UnmappedProperty { get; set; }
        }

        /// <summary>
        /// Base attribute class.
        /// </summary>
        internal class BaseAttribute : ObjectAttribute
        {
            /// <summary>
            /// Gets or sets the first attribute member.
            /// </summary>
            [AttributeMember]
            public string Property1 { get; set; }
        }

        /// <summary>
        /// Derived attribute class.
        /// </summary>
        internal class DerivedAttribute : BaseAttribute
        {
            /// <summary>
            /// Gets or sets the second attribute member.
            /// </summary>
            [AttributeMember]
            public string Property2 { get; set; }
        }
    }
}