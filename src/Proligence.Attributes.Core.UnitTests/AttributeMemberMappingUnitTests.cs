﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeMemberMappingUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core.UnitTests
{
    using System;
    using System.Reflection;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="AttributeMemberMapping"/> class.
    /// </summary>
    [TestFixture]
    public class AttributeMemberMappingUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeMemberMapping"/> class throws an
        /// <see cref="ArgumentNullException"/> when the specified property is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateAttributeMemberMappingWhenPropertyNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AttributeMemberMapping(null, "MyMember", true));

            Assert.That(exception.ParamName, Is.EqualTo("property"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeMemberMapping"/> class throws an
        /// <see cref="ArgumentNullException"/> when the specified member name is <c>null</c> or empty.
        /// </summary>
        /// <param name="memberName">The tested member name.</param>
        [TestCase("")]
        [TestCase(null)]
        public void CreateAttributeMemberMappingWhenMemberNameNullOrEmpty(string memberName)
        {
            PropertyInfo propertyInfo = new Mock<PropertyInfo>().Object;

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new AttributeMemberMapping(propertyInfo, memberName, true));

            Assert.That(exception.ParamName, Is.EqualTo("memberName"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="AttributeMemberMapping"/> class correctly initializes the new 
        /// instance when the specified parameters are valid.
        /// </summary>
        [Test]
        public void CreateAttributeMemberMappingWhenValidArgs()
        {
            PropertyInfo propertyInfo = new Mock<PropertyInfo>().Object;
            
            var mapping = new AttributeMemberMapping(propertyInfo, "MyMember", true);

            Assert.That(mapping.Property, Is.SameAs(propertyInfo));
            Assert.That(mapping.MemberName, Is.SameAs("MyMember"));
            Assert.That(mapping.IsReadOnly, Is.True);
        }
    }
}