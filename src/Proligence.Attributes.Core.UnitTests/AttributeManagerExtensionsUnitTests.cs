﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeManagerExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements unit tests for the <see cref="AttributeManagerExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class AttributeManagerExtensionsUnitTests
    {
        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.GetAttribute{TAttribute}"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified attribute manager is <c>null</c>.
        /// </summary>
        [Test]
        public void GetAttributeWhenAttributeManagerNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => AttributeManagerExtensions.GetAttribute<ObjectAttribute>(null, Guid.NewGuid(), "Test"));

            Assert.That(exception.ParamName, Is.EqualTo("attributeManager"));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.GetAttribute{TAttribute}"/> method returns <c>null</c>
        /// if the specified object does not have any attribute with the specified key (name).
        /// </summary>
        [Test]
        public void GetAttributesWhenAttributeDoesNotExist()
        {
            var attributeManager = new Mock<IAttributeManager<ObjectAttribute>>();

            Guid objectId = Guid.NewGuid();
            attributeManager.Setup(x => x.GetObjectAttributes(objectId, "Test")).Returns(new ObjectAttribute[0]);

            ObjectAttribute attribute = attributeManager.Object.GetAttribute(objectId, "Test");

            Assert.That(attribute, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.GetAttribute{TAttribute}"/> method works correctly
        /// when the specified object has a single attribute with the specified key (name).
        /// </summary>
        [Test]
        public void GetAttributeWhenAttributeExists()
        {
            var attributeManager = new Mock<IAttributeManager<ObjectAttribute>>();

            Guid objectId = Guid.NewGuid();

            var expectedAttribute = new ObjectAttribute
            {
                AttributeId = Guid.NewGuid(),
                ObjectId = objectId,
                Key = "Test",
                TextValue = "Value"
            };
            
            attributeManager.Setup(x => x.GetObjectAttributes(objectId, "Test"))
                .Returns(new[] { expectedAttribute });

            ObjectAttribute attribute = attributeManager.Object.GetAttribute(objectId, "Test");

            Assert.That(attribute, Is.SameAs(expectedAttribute));
            attributeManager.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.GetAttribute{TAttribute}"/> method returns the first
        /// found attribute with the specified key of the specified object when the object has multiple attributes
        /// with the specified key.
        /// </summary>
        [Test]
        public void GetAttributeWhenMultipleAttributesExist()
        {
            var attributeManager = new Mock<IAttributeManager<ObjectAttribute>>();

            Guid objectId = Guid.NewGuid();
            
            var expectedAttribute = new ObjectAttribute
            {
                AttributeId = Guid.NewGuid(),
                ObjectId = objectId, 
                Key = "Test", 
                TextValue = "Value"
            };

            attributeManager.Setup(x => x.GetObjectAttributes(objectId, "Test"))
                .Returns(new[]
                    {
                        expectedAttribute,
                        new ObjectAttribute
                        {
                            AttributeId = Guid.NewGuid(), 
                            ObjectId = objectId, 
                            Key = "Test", 
                            TextValue = "Value2"
                        },
                    });

            ObjectAttribute attribute = attributeManager.Object.GetAttribute(objectId, "Test");

            Assert.That(attribute, Is.SameAs(expectedAttribute));
            attributeManager.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.SaveAttribute{TAttribute}"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified attribute manager is <c>null</c>.
        /// </summary>
        [Test]
        public void SaveAttributeWhenAttributeManagerNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => AttributeManagerExtensions.SaveAttribute(null, new ObjectAttribute()));

            Assert.That(exception.ParamName, Is.EqualTo("attributeManager"));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.SaveAttribute{TAttribute}"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified attribute is <c>null</c>.
        /// </summary>
        [Test]
        public void SaveAttributeWhenAttributeNull()
        {
            var attributeManager = new Mock<IAttributeManager<ObjectAttribute>>();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => attributeManager.Object.SaveAttribute(null));

            Assert.That(exception.ParamName, Is.EqualTo("attribute"));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.SaveAttribute{TAttribute}"/> method works properly
        /// when a non-null attribute is specified..
        /// </summary>
        [Test]
        public void SaveAttributeWhenValidArgs()
        {
            var attributeManager = new Mock<IAttributeManager<ObjectAttribute>>();

            Guid objectId = Guid.NewGuid();

            var attribute = new ObjectAttribute
            {
                AttributeId = Guid.NewGuid(),
                ObjectId = objectId,
                Key = "Test",
                TextValue = "Value"
            };

            IEnumerable<ObjectAttribute> savedAttributes = null;
            attributeManager.Setup(x => x.SaveAttributes(It.IsAny<IEnumerable<ObjectAttribute>>(), It.IsAny<bool>()))
                .Callback<IEnumerable<ObjectAttribute>, bool>((x, b) => { savedAttributes = x; });

            attributeManager.Object.SaveAttribute(attribute);

            Assert.That(savedAttributes.Single(), Is.SameAs(attribute));
            attributeManager.VerifyAll();
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.RemoveAttribute{TAttribute}"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified attribute manager is <c>null</c>.
        /// </summary>
        [Test]
        public void RemoveAttributeWhenManagerNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => AttributeManagerExtensions.RemoveAttribute<ObjectAttribute>(null, Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("attributeManager"));
        }

        /// <summary>
        /// Tests if the <see cref="AttributeManagerExtensions.RemoveAttribute{TAttribute}"/> method works correctly
        /// when the specified arguments are valid.
        /// </summary>
        [Test]
        public void RemoveAttributeWhenValidArgs()
        {
            Guid attributeId = Guid.NewGuid();
            var attributeManager = new Mock<IAttributeManager<ObjectAttribute>>();
            
            IEnumerable<Guid> removedAttributes = null;
            attributeManager.Setup(x => x.RemoveAttributes(It.IsAny<IEnumerable<Guid>>()))
                .Callback<IEnumerable<Guid>>(x => { removedAttributes = x; });

            attributeManager.Object.RemoveAttribute(attributeId);

            Assert.That(removedAttributes.Single(), Is.EqualTo(attributeId));
        }
    }
}