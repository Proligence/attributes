﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringTable.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Sql
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// Implements access to a table which maps strings to integer identifiers.
    /// </summary>
    public class StringTable : IStringTable, IDisposable
    {
        /// <summary>
        /// The connection to the SQL server database which stores the string table.
        /// </summary>
        private readonly SqlConnection connection;

        /// <summary>
        /// The SQL command used to get the strings and their identifiers from the string table.
        /// </summary>
        private readonly SqlCommand getStringCommand;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringTable"/> class.
        /// </summary>
        /// <param name="serverName">The name of the SQL server which stores the string table.</param>
        /// <param name="databaseName">The name of the SQL database which stores the string table.</param>
        public StringTable(string serverName, string databaseName)
        {
            if (serverName == null)
            {
                throw new ArgumentNullException("serverName");
            }

            if (databaseName == null)
            {
                throw new ArgumentNullException("databaseName");
            }

            var connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = serverName;
            connectionStringBuilder.InitialCatalog = databaseName;
            connectionStringBuilder.IntegratedSecurity = true;

            this.connection = new SqlConnection(connectionStringBuilder.ToString());

            this.getStringCommand = new SqlCommand("dbo.GetString", this.connection);
            this.getStringCommand.CommandType = CommandType.StoredProcedure;
            this.getStringCommand.Parameters.Add(new SqlParameter("@StringId", SqlDbType.Int))
                .Direction = ParameterDirection.InputOutput;
            this.getStringCommand.Parameters.Add(new SqlParameter("@String", SqlDbType.NVarChar, 4000))
                .Direction = ParameterDirection.InputOutput;
        }

        /// <summary>
        /// Gets the identifier of the specified string.
        /// </summary>
        /// <param name="value">The <see cref="string"/> to get identifier for.</param>
        /// <returns>The identifier of the specified <see cref="string"/>.</returns>
        public int GetStringId(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException("value");
            }

            this.connection.Open();
            try
            {
                this.getStringCommand.Parameters["@StringId"].Value = DBNull.Value;
                this.getStringCommand.Parameters["@String"].Value = value;
                this.getStringCommand.ExecuteNonQuery();

                return (int)this.getStringCommand.Parameters["@StringId"].Value;
            }
            finally
            {
                this.connection.Close();
            }
        }

        /// <summary>
        /// Gets the string corresponding to the specified identifier.
        /// </summary>
        /// <param name="stringId">The identifier of the string to get.</param>
        /// <returns>
        /// The string with the specified identifier or <c>null</c> if the specified identifier is not associated with
        /// any string.
        /// </returns>
        public string GetString(int stringId)
        {
            this.connection.Open();
            try
            {
                this.getStringCommand.Parameters["@StringId"].Value = stringId;
                this.getStringCommand.Parameters["@String"].Value = DBNull.Value;
                this.getStringCommand.ExecuteNonQuery();

                object value = this.getStringCommand.Parameters["@String"].Value;
                
                return Convert.IsDBNull(value) ? null : (string)value;
            }
            finally
            {
                this.connection.Close();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing">
        /// <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged
        /// resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.connection != null)
                {
                    this.connection.Dispose();
                }

                if (this.getStringCommand != null)
                {
                    this.getStringCommand.Dispose();
                }
            }
        }
    }
}