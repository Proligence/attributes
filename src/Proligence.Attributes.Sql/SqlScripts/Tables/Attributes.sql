﻿-----------------------------------------------------------------------------------------------------------------------
--   Copyright (C) Proligence
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.
--
-- For commercial license contact info@proligence.pl.
-----------------------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Attributes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Attributes](
	[ObjectId] [uniqueidentifier] NOT NULL,
	[AttributeKey] [int] NOT NULL,
	[TextValue] [nvarchar](max) COLLATE Polish_CI_AS NULL,
	[XmlValue] [xml] NULL,
	[GuidRefValue] [uniqueidentifier] NULL,
	[AttributeId] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[StringProperty] [varchar](255) COLLATE Polish_CI_AS NULL,
	[Int] [int] NULL,
	[BooleanProperty] [bit] NULL,
 CONSTRAINT [PK_Attributes] PRIMARY KEY NONCLUSTERED 
(
	[AttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

 GO 
 
