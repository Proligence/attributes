﻿-----------------------------------------------------------------------------------------------------------------------
--   Copyright (C) Proligence
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.
--
-- For commercial license contact info@proligence.pl.
-----------------------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetString]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [dbo].[GetString]
	@StringId int output,
	@String nvarchar(4000) output
as
begin

	set nocount on
	
	if(@StringId is not null)
	begin
	
		set @String = null
		
		select @String = Value 
		from StringTable 
		where StringId = @StringId
	
	end else if(@String is not null)
	begin
	
		select @StringId = StringId
		from StringTable
		where Value = @String
		
		if(@StringId is null)
		begin
			insert into StringTable (Value)
			values (@String)
			select @StringId = scope_identity()
		end
	
	end

end

' 
END

 GO 
 
