﻿-----------------------------------------------------------------------------------------------------------------------
--   Copyright (C) Proligence
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.
--
-- For commercial license contact info@proligence.pl.
-----------------------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveAttribute]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SaveAttribute] AS' 
END


ALTER procedure [dbo].[SaveAttribute]
	@AttributeId uniqueidentifier = null output,
	@ObjectId uniqueidentifier,
	@Key nvarchar(4000),
	@TextValue nvarchar(max),
	@XmlValue xml,
	@GuidRefValue uniqueidentifier,
	@OverwriteExisting bit = 0
as
begin

	set nocount on

	declare @AttributeKey int
	exec GetString @AttributeKey output, @Key
  
	if (@AttributeId is null) and (@OverwriteExisting = 1)
	begin
		select top 1 @AttributeId = AttributeId
		from dbo.Attributes
		where AttributeKey = @AttributeKey and ObjectId = @ObjectId
	end
	
	if @AttributeId is not null
	begin
	
		update dbo.Attributes
		set 
			ObjectId = @ObjectId,
			AttributeKey = @AttributeKey,
			TextValue = @TextValue,
			XmlValue = @XmlValue,
			GuidRefValue = @GuidRefValue
		where 
			AttributeId = @AttributeId
			
		select *
		from dbo.AttributesView
		where AttributeId = @AttributeId
	
	end else begin
	
		declare @t table (AttributeId uniqueidentifier)
	
		insert into dbo.Attributes (AttributeId, ObjectId, AttributeKey, TextValue, XmlValue, GuidRefValue)
		output inserted.AttributeId into @t
		values (newid(), @ObjectId, @AttributeKey, @TextValue, @XmlValue, @GuidRefValue)
			
		select top 1 @AttributeId = AttributeId from @t
		
		select *
		from dbo.AttributesView
		where AttributeId = @AttributeId
		
	end

end


 GO  
