﻿SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[help].[fn_GetAttribute]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

CREATE function [help].[fn_GetAttribute] 
(
	@ObjectId uniqueidentifier,
	@AttributeKey nvarchar(255)
)
returns table
as
return
(
	select 
		at.AttributeId,
		at.AttributeKey,
		at.TextValue,
		at.XmlValue,
		at.GuidRefValue
	from
		dbo.Attributes at inner join
		dbo.StringTable st on at.AttributeKey = st.StringId
	where 
		at.ObjectId = @ObjectId and 
		st.Value = @AttributeKey
)

' 
END

 GO  
