﻿-----------------------------------------------------------------------------------------------------------------------
--   Copyright (C) Proligence
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see http://www.gnu.org/licenses/.
--
-- For commercial license contact info@proligence.pl.
-----------------------------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AttributesView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW dbo.AttributesView
AS
SELECT        dbo.Attributes.ObjectId, dbo.StringTable.Value AS AttributeKey, dbo.Attributes.TextValue, dbo.Attributes.XmlValue, dbo.Attributes.GuidRefValue, 
                         dbo.Attributes.AttributeId, dbo.Attributes.StringProperty, dbo.Attributes.Int, dbo.Attributes.BooleanProperty
FROM            dbo.Attributes INNER JOIN
                         dbo.StringTable ON dbo.Attributes.AttributeKey = dbo.StringTable.StringId
' 

 GO 
 
