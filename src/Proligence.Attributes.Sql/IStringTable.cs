﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStringTable.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Sql
{
    /// <summary>
    /// Defines the API for classes which implement access to a table which maps strings to integer identifiers.
    /// </summary>
    public interface IStringTable
    {
        /// <summary>
        /// Gets the identifier of the specified string.
        /// </summary>
        /// <param name="value">The <see cref="string"/> to get identifier for.</param>
        /// <returns>The identifier of the specified <see cref="string"/>.</returns>
        int GetStringId(string value);

        /// <summary>
        /// Gets the string corresponding to the specified identifier.
        /// </summary>
        /// <param name="stringId">The identifier of the string to get.</param>
        /// <returns>
        /// The string with the specified identifier or <c>null</c> if the specified identifier is not associated with
        /// any string.
        /// </returns>
        string GetString(int stringId);
    }
}