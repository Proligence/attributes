﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlStore.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Sql
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements reading and saving attributes from a SQL database. 
    /// </summary>
    public class SqlStore : IAttributeStore, ISetQueryableStore
    {
        /// <summary>
        /// The connection string to the SQL server database which stores the string table.
        /// </summary>
        private readonly string connectionString;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlStore"/> class.
        /// </summary>
        /// <param name="serverName">The name of the SQL server which stores the string table.</param>
        /// <param name="databaseName">The name of the SQL database which stores the string table.</param>
        public SqlStore(string serverName, string databaseName)
        {
            if (serverName == null)
            {
                throw new ArgumentNullException("serverName");
            }

            if (databaseName == null)
            {
                throw new ArgumentNullException("databaseName");
            }

            var connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = serverName;
            connectionStringBuilder.InitialCatalog = databaseName;
            connectionStringBuilder.IntegratedSecurity = true;

            this.connectionString = connectionStringBuilder.ToString();
        }

        /// <summary>
        /// Gets all attributes of the specified object.
        /// </summary>
        /// <param name="objectId">The identifier of the object which attributes will be returned.</param>
        /// <returns>A sequence of attributes associated with the specified object.</returns>
        public IEnumerable<AttributeData> GetAttributes(Guid objectId)
        {
            var attributes = new List<AttributeData>();

            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = new SqlCommand(
                    "SELECT * FROM dbo.AttributesView WHERE ObjectId = @ObjectId",
                    connection))
                {
                    command.Parameters.AddWithValue("@ObjectId", objectId);
                    command.CommandTimeout = 0;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AttributeData attr = GetAttributeFromXmlReader(reader);
                            attributes.Add(attr);
                        }
                    }
                }
            }

            return attributes;
        }

        /// <summary>
        /// Gets the attribute with the specified identifier.
        /// </summary>
        /// <param name="attributeId">The identifier of the attribute to get.</param>
        /// <returns>
        /// The object which represents the specified attribute or <c>null</c> if the specified attribute was not
        /// found.
        /// </returns>
        public AttributeData GetAttribute(Guid attributeId)
        {
            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = new SqlCommand(
                    "SELECT * FROM dbo.AttributesView WHERE AttributeId = @AttributeId",
                    connection))
                {
                    command.Parameters.AddWithValue("@AttributeId", attributeId);
                    command.CommandTimeout = 0;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return GetAttributeFromXmlReader(reader);
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Saves the specified attributes in the store.
        /// </summary>
        /// <param name="attributes">The sequence of attributes to save in the store.</param>
        /// <param name="overwriteExisting"><c>true</c> to overwrite existing attributes with the same key.</param>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities",
            Justification = "By design - it's ok here.")]
        public void SaveAttributes(IEnumerable<AttributeData> attributes, bool overwriteExisting)
        {
            if (attributes == null)
            {
                throw new ArgumentNullException("attributes");
            }

            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();

                using (var command = new SqlCommand("dbo.SaveAttribute", connection))
                {
                    command.CommandTimeout = 0;

                    using (var updateCommand = new SqlCommand(string.Empty, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@AttributeId", SqlDbType.UniqueIdentifier))
                            .Direction = ParameterDirection.InputOutput;

                        command.Parameters.Add(new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier));
                        command.Parameters.Add(new SqlParameter("@Key", SqlDbType.NVarChar));
                        command.Parameters.Add(new SqlParameter("@TextValue", SqlDbType.NVarChar));
                        command.Parameters.Add(new SqlParameter("@XmlValue", SqlDbType.Xml));
                        command.Parameters.Add(new SqlParameter("@GuidRefValue", SqlDbType.UniqueIdentifier));
                        command.Parameters.Add(new SqlParameter("@OverwriteExisting", SqlDbType.Bit));

                        foreach (AttributeData attribute in attributes)
                        {
                            if (attribute.AttributeId != Guid.Empty)
                            {
                                command.Parameters["@AttributeId"].Value = attribute.AttributeId;
                            }
                            else
                            {
                                command.Parameters["@AttributeId"].Value = DBNull.Value;
                            }

                            command.Parameters["@ObjectId"].Value = attribute.ObjectId;
                            command.Parameters["@Key"].Value = attribute.Key;

                            if (attribute.TextValue != null)
                            {
                                command.Parameters["@TextValue"].Value = attribute.TextValue;
                            }
                            else
                            {
                                command.Parameters["@TextValue"].Value = DBNull.Value;
                            }

                            if (attribute.XmlValue != null)
                            {
                                command.Parameters["@XmlValue"].Value = attribute.XmlValue;
                            }
                            else
                            {
                                command.Parameters["@XmlValue"].Value = DBNull.Value;
                            }

                            if (attribute.GuidRefValue != null)
                            {
                                command.Parameters["@GuidRefValue"].Value = attribute.GuidRefValue;
                            }
                            else
                            {
                                command.Parameters["@GuidRefValue"].Value = DBNull.Value;
                            }

                            command.Parameters["@OverwriteExisting"].Value = overwriteExisting ? 1 : 0;

                            command.ExecuteNonQuery();

                            attribute.AttributeId = (Guid)command.Parameters["@AttributeId"].Value;

                            if (attribute.DataMembers.Count > 0)
                            {
                                IEnumerable<string> setListItems = attribute.DataMembers.Select(
                                    x => string.Format(CultureInfo.InvariantCulture, "{0} = @{1}", x.Key, x.Key));
                                string setList = string.Join(",", setListItems);

                                updateCommand.CommandText =
                                    "UPDATE dbo.Attributes SET " + setList + " WHERE AttributeId = @AttributeId";

                                updateCommand.Parameters.Clear();
                                updateCommand.Parameters.AddWithValue("@AttributeId", attribute.AttributeId);

                                foreach (KeyValuePair<string, object> dataMember in attribute.DataMembers)
                                {
                                    updateCommand.Parameters.AddWithValue("@" + dataMember.Key, dataMember.Value);
                                }

                                updateCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Removes attributes with the specified identifiers.
        /// </summary>
        /// <param name="attributeIds">The identifiers of the attributes to remove.</param>
        public void RemoveAttributes(IEnumerable<Guid> attributeIds)
        {
            if (attributeIds == null)
            {
                throw new ArgumentNullException("attributeIds");
            }

            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = new SqlCommand(
                    "DELETE FROM dbo.Attributes WHERE AttributeId = @AttributeId",
                    connection))
                {
                    command.Parameters.Add(new SqlParameter("@AttributeId", SqlDbType.UniqueIdentifier));
                    command.CommandTimeout = 0;
                    connection.Open();

                    foreach (Guid attributeId in attributeIds)
                    {
                        command.Parameters["@AttributeId"].Value = attributeId;
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Gets the identifiers of all objects which are associated with the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the associated attributes or <c>null</c>.</param>
        /// <param name="textValue">The text value of the associated attributes or <c>null</c>.</param>
        /// <param name="refValue">The referenced object value of the associated attributes or <c>null</c>.</param>
        /// <returns>
        /// A sequence of distinct identifiers of the objects that are associated with at least one attribute which
        /// fulfills the specified attribute conditions.
        /// </returns>
        public IEnumerable<Guid> GetObjects(string key, string textValue = null, Guid? refValue = null)
        {
            if (string.IsNullOrEmpty(key) && (textValue == null) && (refValue == null))
            {
                throw new ArgumentException("At least one non-null parameter must be specified.");
            }

            var objectIds = new List<Guid>();
            var parameters = new List<SqlParameter>();

            string query = "SELECT DISTINCT ObjectId FROM dbo.AttributesView WHERE (1 = 1)";
            
            if (!string.IsNullOrEmpty(key))
            {
                query += " AND (AttributeKey = @AttributeKey)";
                parameters.Add(new SqlParameter("@AttributeKey", key));
            }

            if (textValue != null)
            {
                query += " AND (TextValue = @TextValue)";
                parameters.Add(new SqlParameter("@TextValue", textValue));
            }

            if (refValue != null)
            {
                query += " AND (GuidRefValue = @GuidRefValue)";
                parameters.Add(new SqlParameter("@GuidRefValue", refValue.Value));
            }

            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddRange(parameters.ToArray());
                    command.CommandTimeout = 0;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            objectIds.Add(reader.GetGuid(0));
                        }
                    }
                }
            }

            return objectIds;
        }

        /// <summary>
        /// Gets the identifiers of all objects which are associated with the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the associated attributes or <c>null</c>.</param>
        /// <param name="textValues">
        /// The set of text values of the associated attributes or <c>null</c>. If the attribute matches any value
        /// from the list, the associated object identifier will be returned.
        /// </param>
        /// <param name="refValue">The referenced object value of the associated attributes or <c>null</c>.</param>
        /// <returns>
        /// A sequence of distinct identifiers of the objects that are associated with at least one attribute which
        /// fulfills the specified attribute conditions.
        /// </returns>
        /// <remarks>
        /// The limitation of this implementation is that the text values may not contain any commas (<c>,</c>).
        /// </remarks>
        public IEnumerable<Guid> GetObjectsBySet(string key, IEnumerable<string> textValues, Guid? refValue = null)
        {
            IList<string> valuesList = null;
            if (textValues != null)
            {
                valuesList = textValues as IList<string> ?? textValues.ToArray();
            }

            if (string.IsNullOrEmpty(key) && (valuesList == null || !valuesList.Any()) && (refValue == null))
            {
                throw new ArgumentException("At least one non-null parameter must be specified.");
            }

            var objectIds = new List<Guid>();
            var parameters = new List<SqlParameter>();

            string query = "SELECT DISTINCT ObjectId FROM dbo.AttributesView WHERE (1 = 1)";

            if (!string.IsNullOrEmpty(key))
            {
                query += " AND (AttributeKey = @AttributeKey)";
                parameters.Add(new SqlParameter("@AttributeKey", key));
            }

            if ((valuesList != null) && valuesList.Any())
            {
                query += " AND (TextValue in (SELECT * FROM dbo.fn_SplitString(@ValueList, ',')))";
                parameters.Add(new SqlParameter("@ValueList", string.Join(",", valuesList)));
            }

            if (refValue != null)
            {
                query += " AND (GuidRefValue = @GuidRefValue)";
                parameters.Add(new SqlParameter("@GuidRefValue", refValue.Value));
            }

            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddRange(parameters.ToArray());
                    command.CommandTimeout = 0;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            objectIds.Add(reader.GetGuid(0));
                        }
                    }
                }
            }

            return objectIds;
        }

        /// <summary>
        /// Gets the identifiers of all objects referenced by the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the attributes or <c>null</c>.</param>
        /// <param name="textValue">The text value of the associated attributes or <c>null</c>.</param>
        /// <param name="objectId">The identifier of the object associated with the attributes or <c>null</c>.</param>
        /// <returns>A sequence of distinct identifiers of objects referenced from the specified attributes.</returns>
        public IEnumerable<Guid> GetReferencedObjects(string key, string textValue = null, Guid? objectId = null)
        {
            if (string.IsNullOrEmpty(key) && (textValue == null) && (objectId == null))
            {
                throw new ArgumentException("At least one non-null parameter must be specified.");
            }
            
            var objectIds = new List<Guid>();
            var parameters = new List<SqlParameter>();

            string query = "SELECT DISTINCT GuidRefValue FROM dbo.AttributesView WHERE (1 = 1)";

            if (!string.IsNullOrEmpty(key))
            {
                query += " AND (AttributeKey = @AttributeKey)";
                parameters.Add(new SqlParameter("@AttributeKey", key));
            }

            if (textValue != null)
            {
                query += " AND (TextValue = @TextValue)";
                parameters.Add(new SqlParameter("@TextValue", textValue));
            }

            if (objectId != null)
            {
                query += " AND (ObjectId = @ObjectId)";
                parameters.Add(new SqlParameter("@ObjectId", objectId.Value));
            }

            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddRange(parameters.ToArray());
                    command.CommandTimeout = 0;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                objectIds.Add(reader.GetGuid(0));   
                            }
                        }
                    }
                }
            }

            return objectIds;
        }

        /// <summary>
        /// Gets the identifiers of all objects referenced by the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the attributes or <c>null</c>.</param>
        /// <param name="textValues">
        /// The set of text values of the associated attributes or <c>null</c>. If the attribute matches any value
        /// from the list, the associated object identifier will be returned.
        /// </param>
        /// <param name="objectId">The identifier of the object associated with the attributes or <c>null</c>.</param>
        /// <returns>A sequence of distinct identifiers of objects referenced from the specified attributes.</returns>
        /// <remarks>
        /// The limitation of this implementation is that the text values may not contain any commas (<c>,</c>).
        /// </remarks>
        public IEnumerable<Guid> GetReferencedObjectsBySet(
            string key,
            IEnumerable<string> textValues,
            Guid? objectId = null)
        {
            IList<string> valuesList = null;
            if (textValues != null)
            {
                valuesList = textValues as IList<string> ?? textValues.ToArray();
            }

            if (string.IsNullOrEmpty(key) && (valuesList == null || !valuesList.Any()) && (objectId == null))
            {
                throw new ArgumentException("At least one non-null parameter must be specified.");
            }

            var objectIds = new List<Guid>();
            var parameters = new List<SqlParameter>();

            string query = "SELECT DISTINCT GuidRefValue FROM dbo.AttributesView WHERE (1 = 1)";

            if (!string.IsNullOrEmpty(key))
            {
                query += " AND (AttributeKey = @AttributeKey)";
                parameters.Add(new SqlParameter("@AttributeKey", key));
            }

            if ((valuesList != null) && valuesList.Any())
            {
                query += " AND (TextValue in (SELECT * FROM dbo.fn_SplitString(@ValueList, ',')))";
                parameters.Add(new SqlParameter("@ValueList", string.Join(",", valuesList)));
            }

            if (objectId != null)
            {
                query += " AND (ObjectId = @ObjectId)";
                parameters.Add(new SqlParameter("@ObjectId", objectId.Value));
            }

            using (var connection = new SqlConnection(this.connectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddRange(parameters.ToArray());
                    command.CommandTimeout = 0;
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                objectIds.Add(reader.GetGuid(0));
                            }
                        }
                    }
                }
            }

            return objectIds;
        }

        /// <summary>
        /// Reads an object attribute from the specified <see cref="SqlDataReader"/>.
        /// </summary>
        /// <param name="reader">The <see cref="SqlDataReader"/> object.</param>
        /// <returns>The object attribute read from the current row of the specified reader.</returns>
        private static AttributeData GetAttributeFromXmlReader(SqlDataReader reader)
        {
            var attr = new AttributeData();

            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.IsDBNull(i))
                {
                    continue;
                }

                string name = reader.GetName(i);
                switch (name)
                {
                    case "AttributeId":
                        attr.AttributeId = (Guid)reader.GetValue(i);
                        break;

                    case "ObjectId":
                        attr.ObjectId = (Guid)reader.GetValue(i);
                        break;

                    case "AttributeKey":
                        attr.Key = reader.GetString(i);
                        break;

                    case "TextValue":
                        attr.TextValue = reader.GetString(i);
                        break;

                    case "XmlValue":
                        attr.XmlValue = reader.GetString(i);
                        break;

                    case "GuidRefValue":
                        attr.GuidRefValue = (Guid)reader.GetValue(i);
                        break;

                    default:
                        attr.DataMembers.Add(name, reader.GetValue(i));
                        break;
                }
            }
            
            return attr;
        }
    }
}