﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectAttributeUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Common.UnitTests
{
    using System;
    using System.Xml;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="ObjectAttribute"/> class.
    /// </summary>
    [TestFixture]
    public class ObjectAttributeUnitTests
    {
        /// <summary>
        /// Tests if the XML values are correctly saved in the attribute object.
        /// </summary>
        [Test]
        public void SetXmlValue()
        {
            string xml = "<Root><Child>Text1</Child><Child>Text1</Child></Root>";

            var document = new XmlDocument();
            document.LoadXml(xml);

            var attr = new ObjectAttributeMock();
            attr.XmlValue = document;

            Assert.That(RemoveWhitespace(attr.Xml), Is.EqualTo(xml));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.XmlValue"/> property can be correctly set to <c>null</c>.
        /// </summary>
        [Test]
        public void SetXmlValueNull()
        {
            var attr = new ObjectAttributeMock();
            attr.XmlValue = null;

            Assert.That(attr.Xml, Is.Null);
        }

        /// <summary>
        /// Tests if the XML values are correctly read from the attribute object.
        /// </summary>
        [Test]
        public void GetXmlValue()
        {
            string xml = "<Root><Child>Text1</Child><Child>Text1</Child></Root>";

            var attr = new ObjectAttributeMock();
            attr.Xml = xml;

            string actualXml = attr.XmlValue.CreateNavigator().OuterXml;
            Assert.That(RemoveWhitespace(actualXml), Is.EqualTo(xml));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.Equals(object)"/> method returns <c>false</c> if the specified
        /// argument is <c>null</c>.
        /// </summary>
        [Test]
        public void EqualsNull()
        {
            var attr = new ObjectAttribute();

            Assert.That(attr.Equals(null), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.Equals(object)"/> method returns <c>false</c> if the type of the 
        /// specified argument is not <see cref="ObjectAttribute"/>.
        /// </summary>
        [Test]
        public void EqualsOtherType()
        {
            var attr = new ObjectAttribute();

            object obj = new DateTime();
            Assert.That(attr.Equals(obj), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.Equals(object)"/> method returns <c>true</c> if the specified
        /// argument is equal to the current <see cref="ObjectAttribute"/>.
        /// </summary>
        [Test]
        public void EqualsWhenEqual()
        {
            var attr1 = new ObjectAttribute { AttributeId = Guid.NewGuid() };
            var attr2 = new ObjectAttribute { AttributeId = attr1.AttributeId };

            Assert.That(attr1.Equals(attr2), Is.True);
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.Equals(object)"/> method returns <c>false</c> if the specified
        /// argument is not equal to the current <see cref="ObjectAttribute"/>.
        /// </summary>
        [Test]
        public void EqualsWhenNotEqual()
        {
            var attr1 = new ObjectAttribute { AttributeId = Guid.NewGuid() };
            var attr2 = new ObjectAttribute { AttributeId = Guid.NewGuid() };

            Assert.That(attr1.Equals(attr2), Is.False);
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.GetHashCode"/> method returns the same value for attributes which
        /// have the same the same identifier.
        /// </summary>
        [Test]
        public void HashCodeIsEqualForEqualAttributes()
        {
            var attr1 = new ObjectAttribute { AttributeId = Guid.NewGuid() };
            var attr2 = new ObjectAttribute { AttributeId = attr1.AttributeId };

            Assert.That(attr1.GetHashCode(), Is.EqualTo(attr2.GetHashCode()));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.ToString"/> method calls the base implementation if the attribute's
        /// key is <c>null</c>.
        /// </summary>
        [Test]
        public void ToStringWhenKeyNull()
        {
            var attr = new ObjectAttribute { Key = null };

            Assert.That(attr.ToString(), Is.EqualTo("Proligence.Attributes.Common.ObjectAttribute"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.ToString"/> method calls the base implementation if the attribute's
        /// key is an empty string.
        /// </summary>
        [Test]
        public void ToStringWhenKeyEmpty()
        {
            var attr = new ObjectAttribute { Key = string.Empty };

            Assert.That(attr.ToString(), Is.EqualTo("Proligence.Attributes.Common.ObjectAttribute"));
        }

        /// <summary>
        /// Tests if the <see cref="ObjectAttribute.ToString"/> method calls the base implementation if the attribute's
        /// key contains only whitespace.
        /// </summary>
        [Test]
        public void ToStringWhenKeyWhiteSpace()
        {
            var attr = new ObjectAttribute { Key = " \t\r\n" };

            Assert.That(attr.ToString(), Is.EqualTo("Proligence.Attributes.Common.ObjectAttribute"));
        }

        /// <summary>
        /// Tests the value returned by the <see cref="ObjectAttribute.ToString"/> method when the attribute does not
        /// contain any value.
        /// </summary>
        [Test]
        public void ToStringWhenNoValue()
        {
            var objectId = Guid.NewGuid();
            var attr = new ObjectAttribute
            {
                Key = "MyKey",
                ObjectId = objectId
            };

            Assert.That(attr.ToString(), Is.EqualTo("MyKey : " + objectId + " ->"));
        }

        /// <summary>
        /// Tests the value returned by the <see cref="ObjectAttribute.ToString"/> method when the attribute contains
        /// a text value.
        /// </summary>
        [Test]
        public void ToStringWhenStringValue()
        {
            var objectId = Guid.NewGuid();
            var attr = new ObjectAttribute
            {
                Key = "MyKey",
                ObjectId = objectId,
                TextValue = "MyValue"
            };

            Assert.That(attr.ToString(), Is.EqualTo("MyKey : " + objectId + " -> T[MyValue]"));
        }

        /// <summary>
        /// Tests the value returned by the <see cref="ObjectAttribute.ToString"/> method when the attribute contains
        /// a XML value.
        /// </summary>
        [Test]
        public void ToStringWhenXmlValue()
        {
            var objectId = Guid.NewGuid();

            var value = new XmlDocument();
            value.LoadXml("<value>MyValue</value>");

            var attr = new ObjectAttribute
            {
                Key = "MyKey",
                ObjectId = objectId,
                XmlValue = value
            };

            Assert.That(attr.ToString(), Is.EqualTo("MyKey : " + objectId + " -> X[<value>MyValue</value>]"));
        }

        /// <summary>
        /// Tests the value returned by the <see cref="ObjectAttribute.ToString"/> method when the attribute contains
        /// a GUID reference value.
        /// </summary>
        [Test]
        public void ToStringWhenRefValue()
        {
            var objectId = Guid.NewGuid();
            var value = Guid.NewGuid();
            var attr = new ObjectAttribute
            {
                Key = "MyKey",
                ObjectId = objectId,
                GuidRefValue = value
            };

            Assert.That(attr.ToString(), Is.EqualTo("MyKey : " + objectId + " -> R[" + value + "]"));
        }

        /// <summary>
        /// Tests the value returned by the <see cref="ObjectAttribute.ToString"/> method when the attribute contains
        /// multiple values.
        /// </summary>
        [Test]
        public void ToStringWhenMultipleValues()
        {
            var objectId = Guid.NewGuid();
            var value = Guid.NewGuid();
            var attr = new ObjectAttribute
            {
                Key = "MyKey",
                ObjectId = objectId,
                TextValue = "MyValue",
                GuidRefValue = value
            };

            Assert.That(attr.ToString(), Is.EqualTo("MyKey : " + objectId + " -> T[MyValue] R[" + value + "]"));
        }

        /// <summary>
        /// Removes all whitespace from the specified <see cref="string"/>.
        /// </summary>
        /// <param name="str">The string from which whitespace will be removed.</param>
        /// <returns>The result string.</returns>
        private static string RemoveWhitespace(string str)
        {
            if (str != null)
            {
                str = str
                    .Replace(" ", string.Empty)
                    .Replace("\r", string.Empty)
                    .Replace("\n", string.Empty)
                    .Replace("\t", string.Empty);
            }

            return str;
        }

        /// <summary>
        /// Implements a subclass of <see cref="ObjectAttribute"/> for test purposes.
        /// </summary>
        private class ObjectAttributeMock : ObjectAttribute
        {
            /// <summary>
            /// Gets or sets the raw XML value associated with the attribute.
            /// </summary>
            public string Xml
            {
                get { return this.RawXmlValue; }
                set { this.RawXmlValue = value; }
            }
        }
    }
}