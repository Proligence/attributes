﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DetachedCriteriaExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.NHibernate.UnitTests
{
    using System;
    using System.Reflection;
    using global::NHibernate.Criterion;
    using global::NHibernate.Impl;
    using global::NHibernate.SqlCommand;
    using NUnit.Framework;

    /// <summary>
    /// Implement unit tests for the <see cref="DetachedCriteriaExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class DetachedCriteriaExtensionsUnitTests
    {
        /// <summary>
        /// Tests if the 
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string)"/> method throws
        /// an <see cref="ArgumentNullException"/> when the specified <see cref="DetachedCriteria"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyWhenCriteriaNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => DetachedCriteriaExtensions.AddAttributeRestriction(null, "Key"));

            Assert.That(exception.ParamName, Is.EqualTo("criteria"));
        }

        /// <summary>
        /// Tests if the <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string)"/>
        /// method throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or
        /// empty.
        /// </summary>
        /// <param name="key">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyWhenKeyNullOrEmpty(string key)
        {
            DetachedCriteria criteria = DetachedCriteria.For<string>();
            
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction(key));

            Assert.That(exception.ParamName, Is.EqualTo("key"));
        }

        /// <summary>
        /// Tests if the <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string)"/>
        /// method works correctly when the specified parameters are valid.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyWhenValidArgs()
        {
            DetachedCriteria criteria = DetachedCriteria.For<string>();
            
            criteria.AddAttributeRestriction("MyKey");

            Assert.That(criteria, Is.SameAs(criteria));

            CriteriaImpl criteriaImpl = GetCriteriaFromDetachedCriteria(criteria);
            CriteriaImpl.Subcriteria attributesCriteria = 
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("Attributes");
            Assert.That(attributesCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            CriteriaImpl.Subcriteria stringTableCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("AttributeKeyEntry");
            Assert.That(stringTableCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            Assert.That(criteria.ToString(), Is.EqualTo("DetachableCriteria(Value = MyKey)"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,string)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified <see cref="DetachedCriteria"/> is
        /// <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyValueWhenCriteriaNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => DetachedCriteriaExtensions.AddAttributeRestriction(null, "Key", "Value"));

            Assert.That(exception.ParamName, Is.EqualTo("criteria"));
        }

        /// <summary>
        /// Tests if the 
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,string)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="key">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyValueWhenKeyNullOrEmpty(string key)
        {
            DetachedCriteria criteria = DetachedCriteria.For<string>();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction(key, "value"));

            Assert.That(exception.ParamName, Is.EqualTo("key"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,string)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="value">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyValueWhenTextValueNullOrEmpty(string value)
        {
            DetachedCriteria criteria = DetachedCriteria.For<string>();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction("key", value));

            Assert.That(exception.ParamName, Is.EqualTo("textValue"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,string)"/> method
        /// works correctly when the specified parameters are valid.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyValueWhenValidArgs()
        {
            DetachedCriteria criteria = DetachedCriteria.For<string>();
                
            criteria.AddAttributeRestriction("MyKey", "Value");

            Assert.That(criteria, Is.SameAs(criteria));

            CriteriaImpl criteriaImpl = GetCriteriaFromDetachedCriteria(criteria);
            CriteriaImpl.Subcriteria attributesCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("Attributes");
            Assert.That(attributesCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            CriteriaImpl.Subcriteria stringTableCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("AttributeKeyEntry");
            Assert.That(stringTableCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            Assert.That(criteria.ToString(), Is.EqualTo("DetachableCriteria(TextValue = Value and Value = MyKey)"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,string,Guid)"/>
        /// method throws an <see cref="ArgumentNullException"/> when the specified <see cref="DetachedCriteria"/> is 
        /// <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyValueRefWhenCriteriaNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => DetachedCriteriaExtensions.AddAttributeRestriction(null, "Key", "Value", Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("criteria"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,string,Guid)"/>
        /// method throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or
        /// empty.
        /// </summary>
        /// <param name="key">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyValueRefWhenKeyNullOrEmpty(string key)
        {
            DetachedCriteria criteria = DetachedCriteria.For<string>();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction(key, "value", Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("key"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,string,Guid)"/>
        /// method throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or
        /// empty.
        /// </summary>
        /// <param name="value">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyValueRefWhenTextValueNullOrEmpty(string value)
        {
            DetachedCriteria criteria = DetachedCriteria.For<string>();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction("key", value, Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("textValue"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,string,Guid)"/>
        /// method works correctly when the specified parameters are valid.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyValueRefWhenValidArgs()
        {
            string refValue = "4652006c-efd0-42c8-8142-37129309776f";

            DetachedCriteria criteria = DetachedCriteria.For<string>();
            
            criteria.AddAttributeRestriction("MyKey", "Value", Guid.Parse(refValue));

            Assert.That(criteria, Is.SameAs(criteria));

            CriteriaImpl criteriaImpl = GetCriteriaFromDetachedCriteria(criteria);
            CriteriaImpl.Subcriteria attributesCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("Attributes");
            Assert.That(attributesCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            CriteriaImpl.Subcriteria stringTableCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("AttributeKeyEntry");
            Assert.That(stringTableCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            string expected =
                "DetachableCriteria(TextValue = Value and GuidRefValue = Guid@-1635630389(hash) " +
                "and Value = MyKey)";
            Assert.That(criteria.ToString(), Is.EqualTo(expected));
        }

        /// <summary>
        /// Tests if the <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,Guid)"/>
        /// method throws an <see cref="ArgumentNullException"/> when the specified <see cref="DetachedCriteria"/> is
        /// <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyRefWhenCriteriaNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => DetachedCriteriaExtensions.AddAttributeRestriction(null, "Key", Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("criteria"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,Guid)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="key">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyRefWhenKeyNullOrEmpty(string key)
        {
            DetachedCriteria criteria = DetachedCriteria.For<string>();

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction(key, Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("key"));
        }

        /// <summary>
        /// Tests if the
        /// <see cref="DetachedCriteriaExtensions.AddAttributeRestriction(DetachedCriteria,string,Guid)"/> method
        /// works correctly when the specified parameters are valid.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyRefWhenValidArgs()
        {
            string refValue = "4652006c-efd0-42c8-8142-37129309776f";

            DetachedCriteria criteria = DetachedCriteria.For<string>();
            
            criteria.AddAttributeRestriction("MyKey", Guid.Parse(refValue));

            Assert.That(criteria, Is.SameAs(criteria));

            CriteriaImpl criteriaImpl = GetCriteriaFromDetachedCriteria(criteria);
            CriteriaImpl.Subcriteria attributesCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("Attributes");
            Assert.That(attributesCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            CriteriaImpl.Subcriteria stringTableCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("AttributeKeyEntry");
            Assert.That(stringTableCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            string expected =
                "DetachableCriteria(GuidRefValue = Guid@-1635630389(hash) and Value = MyKey)";
            Assert.That(criteria.ToString(), Is.EqualTo(expected));
        }

        /// <summary>
        /// Gets the <see cref="CriteriaImpl"/> for the specified <see cref="DetachedCriteria"/>.
        /// </summary>
        /// <param name="criteria">The <see cref="DetachedCriteria"/> object.</param>
        /// <returns>The <see cref="CriteriaImpl"/> object of the <see cref="DetachedCriteria"/>.</returns>
        private static CriteriaImpl GetCriteriaFromDetachedCriteria(DetachedCriteria criteria)
        {
            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic;
            FieldInfo fieldInfo = typeof(DetachedCriteria).GetField("impl", bindingFlags);

            /* ReSharper disable PossibleNullReferenceException */
            CriteriaImpl criteriaImpl = (CriteriaImpl)fieldInfo.GetValue(criteria);
            /* ReSharper restore PossibleNullReferenceException */
            
            return criteriaImpl;
        }
    }
}