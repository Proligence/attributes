﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CriteriaExtensionsUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.NHibernate.UnitTests
{
    using System;
    using Moq;
    using global::NHibernate;
    using global::NHibernate.Engine;
    using global::NHibernate.Impl;
    using global::NHibernate.SqlCommand;
    using NUnit.Framework;

    /// <summary>
    /// Implement unit tests for the <see cref="CriteriaExtensions"/> class.
    /// </summary>
    [TestFixture]
    public class CriteriaExtensionsUnitTests
    {
        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string)"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified <see cref="ICriteria"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyWhenCriteriaNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => CriteriaExtensions.AddAttributeRestriction(null, "Key"));

            Assert.That(exception.ParamName, Is.EqualTo("criteria"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string)"/> method throws an
        /// <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="key">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyWhenKeyNullOrEmpty(string key)
        {
            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object);
            
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction(key));

            Assert.That(exception.ParamName, Is.EqualTo("key"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string)"/> method works
        /// correctly when the specified parameters are valid.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyWhenValidArgs()
        {
            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object)
                .AddAttributeRestriction("MyKey");

            Assert.That(criteria, Is.SameAs(criteria));

            CriteriaImpl criteriaImpl = (CriteriaImpl)criteria;
            CriteriaImpl.Subcriteria attributesCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("Attributes");
            Assert.That(attributesCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            CriteriaImpl.Subcriteria stringTableCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("AttributeKeyEntry");
            Assert.That(stringTableCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            Assert.That(criteria.ToString(), Is.EqualTo("Value = MyKey"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,string)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified <see cref="ICriteria"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyValueWhenCriteriaNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => CriteriaExtensions.AddAttributeRestriction(null, "Key", "Value"));

            Assert.That(exception.ParamName, Is.EqualTo("criteria"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,string)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="key">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyValueWhenKeyNullOrEmpty(string key)
        {
            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction(key, "value"));

            Assert.That(exception.ParamName, Is.EqualTo("key"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,string)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="value">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyValueWhenTextValueNullOrEmpty(string value)
        {
            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction("key", value));

            Assert.That(exception.ParamName, Is.EqualTo("textValue"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,string)"/> method works
        /// correctly when the specified parameters are valid.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyValueWhenValidArgs()
        {
            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object)
                .AddAttributeRestriction("MyKey", "Value");

            Assert.That(criteria, Is.SameAs(criteria));

            CriteriaImpl criteriaImpl = (CriteriaImpl)criteria;
            CriteriaImpl.Subcriteria attributesCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("Attributes");
            Assert.That(attributesCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            CriteriaImpl.Subcriteria stringTableCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("AttributeKeyEntry");
            Assert.That(stringTableCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            Assert.That(criteria.ToString(), Is.EqualTo("TextValue = Value and Value = MyKey"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,string,Guid)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified <see cref="ICriteria"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyValueRefWhenCriteriaNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => CriteriaExtensions.AddAttributeRestriction(null, "Key", "Value", Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("criteria"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,string,Guid)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="key">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyValueRefWhenKeyNullOrEmpty(string key)
        {
            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction(key, "value", Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("key"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,string,Guid)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="value">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyValueRefWhenTextValueNullOrEmpty(string value)
        {
            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction("key", value, Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("textValue"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,string,Guid)"/> method
        /// works correctly when the specified parameters are valid.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyValueRefWhenValidArgs()
        {
            string refValue = "4652006c-efd0-42c8-8142-37129309776f";

            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object)
                .AddAttributeRestriction("MyKey", "Value", Guid.Parse(refValue));

            Assert.That(criteria, Is.SameAs(criteria));

            CriteriaImpl criteriaImpl = (CriteriaImpl)criteria;
            CriteriaImpl.Subcriteria attributesCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("Attributes");
            Assert.That(attributesCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            CriteriaImpl.Subcriteria stringTableCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("AttributeKeyEntry");
            Assert.That(stringTableCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            string expected =
                "TextValue = Value and GuidRefValue = Guid@-1635630389(hash) and Value = MyKey";
            Assert.That(criteria.ToString(), Is.EqualTo(expected));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,Guid)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified <see cref="ICriteria"/> is <c>null</c>.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyRefWhenCriteriaNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => CriteriaExtensions.AddAttributeRestriction(null, "Key", Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("criteria"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,Guid)"/> method
        /// throws an <see cref="ArgumentNullException"/> when the specified attribute key is <c>null</c> or empty.
        /// </summary>
        /// <param name="key">The attribute key to test.</param>
        [TestCase(null)]
        [TestCase("")]
        public void AddAttributeRestrictionKeyRefWhenKeyNullOrEmpty(string key)
        {
            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => criteria.AddAttributeRestriction(key, Guid.NewGuid()));

            Assert.That(exception.ParamName, Is.EqualTo("key"));
        }

        /// <summary>
        /// Tests if the <see cref="CriteriaExtensions.AddAttributeRestriction(ICriteria,string,Guid)"/> method
        /// works correctly when the specified parameters are valid.
        /// </summary>
        [Test]
        public void AddAttributeRestrictionKeyRefWhenValidArgs()
        {
            string refValue = "4652006c-efd0-42c8-8142-37129309776f";

            ICriteria criteria = new CriteriaImpl("TypeName", new Mock<ISessionImplementor>().Object)
                .AddAttributeRestriction("MyKey", Guid.Parse(refValue));

            Assert.That(criteria, Is.SameAs(criteria));

            CriteriaImpl criteriaImpl = (CriteriaImpl)criteria;
            CriteriaImpl.Subcriteria attributesCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("Attributes");
            Assert.That(attributesCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            CriteriaImpl.Subcriteria stringTableCriteria =
                (CriteriaImpl.Subcriteria)criteriaImpl.GetCriteriaByPath("AttributeKeyEntry");
            Assert.That(stringTableCriteria.JoinType, Is.EqualTo(JoinType.InnerJoin));

            Assert.That(
                criteria.ToString(),
                Is.EqualTo("GuidRefValue = Guid@-1635630389(hash) and Value = MyKey"));
        }
    }
}