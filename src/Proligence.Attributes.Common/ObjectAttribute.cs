﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectAttribute.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Common
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Serialization;
    using System.Xml;
    using System.Xml.XPath;

    /// <summary>
    /// Represents an attribute which can be attached to other objects.
    /// </summary>
    [DataContract]
    [SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix",
        Justification = "By design")]
    public class ObjectAttribute
    {
        /// <summary>
        /// The <see cref="IXPathNavigable"/> object for the XML value associated with the attribute.
        /// </summary>
        private IXPathNavigable xmlValue;

        /// <summary>
        /// Gets or sets the unique identifier of the attribute.
        /// </summary>
        [DataMember]
        public Guid AttributeId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the objects for which the attribute is defined.
        /// </summary>
        [DataMember]
        public Guid ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the name of the attribute.
        /// </summary>
        [DataMember]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the textual value of the attribute.
        /// </summary>
        [DataMember]
        public string TextValue { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the object referenced by the attribute.
        /// </summary>
        [DataMember]
        public virtual Guid? GuidRefValue { get; set; }

        /// <summary>
        /// Gets or sets the XML value stored in the attribute.
        /// </summary>
        public IXPathNavigable XmlValue
        {
            get
            {
                if (this.xmlValue == null)
                {
                    this.xmlValue = this.CreateXmlValue();
                }

                return this.xmlValue;
            }

            set
            {
                this.RawXmlValue = this.GetRawXmlValue(value);
            }
        }

        /// <summary>
        /// Gets or sets the raw XML value associated with the attribute.
        /// </summary>
        [DataMember]
        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        protected string RawXmlValue { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="Object"/> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var other = obj as ObjectAttribute;
            if (other != null)
            {
                return this.AttributeId == other.AttributeId;
            }

            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash 
        /// table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.AttributeId.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="String"/> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="String"/> that represents this instance.</returns>
        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(this.Key))
            {
                return base.ToString();
            }

            string str = this.Key + " : " + this.ObjectId + " -> ";

            if (!string.IsNullOrEmpty(this.TextValue))
            {
                str += "T[" + this.TextValue + "] ";
            }

            if (this.XmlValue != null)
            {
                XPathNavigator navigator = this.XmlValue.CreateNavigator();
                str += "X[" + navigator.OuterXml + "] ";
            }

            if (this.GuidRefValue != null)
            {
                str += "R[" + this.GuidRefValue + "] ";
            }

            return str.Trim();
        }

        /// <summary>
        /// Creates a <see cref="IXPathNavigable"/> object which contains the XML document stored in
        /// <see cref="RawXmlValue"/>.
        /// </summary>
        /// <returns>The created <see cref="IXPathNavigable"/> object or <c>null</c>.</returns>
        protected virtual IXPathNavigable CreateXmlValue()
        {
            if (this.RawXmlValue == null)
            {
                return null;
            }

            var document = new XmlDocument();
            document.LoadXml(this.RawXmlValue);

            return document;
        }

        /// <summary>
        /// Gets a <see cref="string"/> which contains the XML value of the specified <see cref="IXPathNavigable"/>
        /// object.
        /// </summary>
        /// <param name="value">The <see cref="IXPathNavigable"/> object from which to extract XML.</param>
        /// <returns>The XML of the specified <see cref="IXPathNavigable"/> or <c>null</c>.</returns>
        protected virtual string GetRawXmlValue(IXPathNavigable value)
        {
            if (value == null)
            {
                return null;
            }

            return value.CreateNavigator().OuterXml;
        }
    }
}
