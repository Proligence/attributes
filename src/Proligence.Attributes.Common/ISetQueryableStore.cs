﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISetQueryableStore.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Common
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the extended API for attribute stores which support querying attributes with values from a set of 
    /// specified values.
    /// </summary>
    public interface ISetQueryableStore
    {
        /// <summary>
        /// Gets the identifiers of all objects which are associated with the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the associated attributes or <c>null</c>.</param>
        /// <param name="textValues">
        /// The set of text values of the associated attributes or <c>null</c>. If the attribute matches any value
        /// from the list, the associated object identifier will be returned.
        /// </param>
        /// <param name="refValue">The referenced object value of the associated attributes or <c>null</c>.</param>
        /// <returns>
        /// A sequence of distinct identifiers of the objects that are associated with at least one attribute which
        /// fulfills the specified attribute conditions.
        /// </returns>
        IEnumerable<Guid> GetObjectsBySet(string key, IEnumerable<string> textValues, Guid? refValue);

        /// <summary>
        /// Gets the identifiers of all objects referenced by the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the attributes or <c>null</c>.</param>
        /// <param name="textValues">
        /// The set of text values of the associated attributes or <c>null</c>. If the attribute matches any value
        /// from the list, the associated object identifier will be returned.
        /// </param>
        /// <param name="objectId">The identifier of the object associated with the attributes or <c>null</c>.</param>
        /// <returns>A sequence of distinct identifiers of objects referenced from the specified attributes.</returns>
        IEnumerable<Guid> GetReferencedObjectsBySet(string key, IEnumerable<string> textValues, Guid? objectId);
    }
}