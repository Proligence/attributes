﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeMemberAttribute.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Common
{
    using System;

    /// <summary>
    /// Marks that a property of an <see cref="ObjectAttribute"/> subclass stores an attribute member value which
    /// should be stored with the attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public sealed class AttributeMemberAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the name of the attribute member in the attribute store.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute member should not be saved or updated in the
        /// attribute store.
        /// </summary>
        public bool IsReadOnly { get; set; }
    }
}