﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeData.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Common
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents an attribute at the attribute store level.
    /// </summary>
    public sealed class AttributeData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeData" /> class.
        /// </summary>
        public AttributeData()
        {
            this.DataMembers = new Dictionary<string, object>();
        }

        /// <summary>
        /// Gets or sets the unique identifier of the attribute.
        /// </summary>
        public Guid AttributeId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the objects for which the attribute is defined.
        /// </summary>
        public Guid ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the name of the attribute.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the textual value of the attribute.
        /// </summary>
        public string TextValue { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the object referenced by the attribute.
        /// </summary>
        public Guid? GuidRefValue { get; set; }

        /// <summary>
        /// Gets or sets the XML value stored in the attribute.
        /// </summary>
        public string XmlValue { get; set; }

        /// <summary>
        /// Gets the data members of the attribute.
        /// </summary>
        public IDictionary<string, object> DataMembers { get; private set; } 
    }
}