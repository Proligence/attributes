﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAttributeManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Common
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the API for classes which manage reading and saving object attributes.
    /// </summary>
    /// <typeparam name="TAttribute">The type which represents the managed object attributes.</typeparam>
    public interface IAttributeManager<TAttribute>
        where TAttribute : ObjectAttribute
    {
        /// <summary>
        /// Gets all attributes of the specified object.
        /// </summary>
        /// <param name="objectId">The identifier of the object which attributes will be returned.</param>
        /// <returns>A sequence of attributes associated with the specified object.</returns>
        IEnumerable<TAttribute> GetObjectAttributes(Guid objectId);

        /// <summary>
        /// Gets all attributes with the specified key of the specified object.
        /// </summary>
        /// <param name="objectId">The identifier of the object which attributes will be returned.</param>
        /// <param name="key">The key (name) of the attributes to get.</param>
        /// <returns>A sequence of attributes associated with the specified object.</returns>
        IEnumerable<TAttribute> GetObjectAttributes(Guid objectId, string key);

        /// <summary>
        /// Gets all attributes of the specified object which satisfy the specified criteria.
        /// </summary>
        /// <param name="objectId">The identifier of the object which attributes will be returned.</param>
        /// <param name="selector">The delegate which filters the attributes to get.</param>
        /// <returns>A sequence of attributes associated with the specified object.</returns>
        IEnumerable<TAttribute> SelectObjectAttributes(Guid objectId, Func<TAttribute, bool> selector);

        /// <summary>
        /// Gets the attribute with the specified identifier.
        /// </summary>
        /// <param name="attributeId">The identifier of the attribute to get.</param>
        /// <returns>
        /// The object which represents the specified attribute or <c>null</c> if the specified attribute was not
        /// found.
        /// </returns>
        TAttribute GetAttribute(Guid attributeId);

        /// <summary>
        /// Saves the specified attributes in the store.
        /// </summary>
        /// <param name="attributes">The sequence of attributes to save in the store.</param>
        /// <param name="overwriteExisting"><c>true</c> to overwrite existing attributes with the same key.</param>
        void SaveAttributes(IEnumerable<TAttribute> attributes, bool overwriteExisting = false);

        /// <summary>
        /// Removes attributes with the specified identifiers.
        /// </summary>
        /// <param name="attributeIds">The identifiers of the attributes to remove.</param>
        void RemoveAttributes(IEnumerable<Guid> attributeIds);

        /// <summary>
        /// Gets the identifiers of all objects which are associated with the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the associated attributes or <c>null</c>.</param>
        /// <param name="textValue">The text value of the associated attributes or <c>null</c>.</param>
        /// <param name="refValue">The referenced object value of the associated attributes or <c>null</c>.</param>
        /// <returns>
        /// A sequence of distinct identifiers of the objects that are associated with at least one attribute which
        /// fulfills the specified attribute conditions.
        /// </returns>
        IEnumerable<Guid> GetObjects(string key, string textValue = null, Guid? refValue = null);

        /// <summary>
        /// Gets the identifiers of all objects which are associated with the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the associated attributes or <c>null</c>.</param>
        /// <param name="textValues">
        /// The set of text values of the associated attributes or <c>null</c>. If the attribute matches any value
        /// from the list, the associated object identifier will be returned.
        /// </param>
        /// <param name="refValue">The referenced object value of the associated attributes or <c>null</c>.</param>
        /// <returns>
        /// A sequence of distinct identifiers of the objects that are associated with at least one attribute which
        /// fulfills the specified attribute conditions.
        /// </returns>
        IEnumerable<Guid> GetObjectsBySet(string key, IEnumerable<string> textValues, Guid? refValue = null);

        /// <summary>
        /// Gets the identifiers of all objects referenced by the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the attributes or <c>null</c>.</param>
        /// <param name="textValue">The text value of the associated attributes or <c>null</c>.</param>
        /// <param name="objectId">The identifier of the object associated with the attributes or <c>null</c>.</param>
        /// <returns>A sequence of distinct identifiers of objects referenced from the specified attributes.</returns>
        IEnumerable<Guid> GetReferencedObjects(string key, string textValue = null, Guid? objectId = null);

        /// <summary>
        /// Gets the identifiers of all objects referenced by the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the attributes or <c>null</c>.</param>
        /// <param name="textValues">
        /// The set of text values of the associated attributes or <c>null</c>. If the attribute matches any value
        /// from the list, the associated object identifier will be returned.
        /// </param>
        /// <param name="objectId">The identifier of the object associated with the attributes or <c>null</c>.</param>
        /// <returns>A sequence of distinct identifiers of objects referenced from the specified attributes.</returns>
        IEnumerable<Guid> GetReferencedObjectsBySet(
            string key,
            IEnumerable<string> textValues,
            Guid? objectId = null);
    }
}