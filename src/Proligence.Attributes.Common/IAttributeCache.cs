﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAttributeCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Common
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the API for classes which implement attribute-per-object caching.
    /// </summary>
    /// <typeparam name="TAttribute">The type which represents the cached object attributes.</typeparam>
    public interface IAttributeCache<TAttribute>
        where TAttribute : ObjectAttribute
    {
        /// <summary>
        /// Gets the cached attributes of the specified object or invokes the specified delegate and caches its result.
        /// </summary>
        /// <param name="objectId">The identifier of the object for which attributes will be returned.</param>
        /// <param name="func">The delegate which gets the object's attributes in case they are not cached.</param>
        /// <returns>The attributes of the specified object.</returns>
        IEnumerable<TAttribute> AddOrGetAttributes(Guid objectId, Func<IEnumerable<TAttribute>> func);

        /// <summary>
        /// Adds the specified attribute to the cache.
        /// </summary>
        /// <param name="objectId">
        /// The identifier of the object for which the attributes will be added to the cache.
        /// </param>
        /// <param name="attributes">The attributes which will be added to the cache.</param>
        void AddAttributes(Guid objectId, IEnumerable<TAttribute> attributes);

        /// <summary>
        /// Removes all cached attributes of the specified object.
        /// </summary>
        /// <param name="objectId">
        /// The identifier of the object which attributes will be removed from the cache.
        /// </param>
        void RemoveAttributes(Guid objectId);
    }
}