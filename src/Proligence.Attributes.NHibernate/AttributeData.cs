﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeData.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.NHibernate
{
    using System;

    /// <summary>
    /// Represents the data of an attribute attached to an object.
    /// </summary>
    public class AttributeData
    {
        /// <summary>
        /// Gets or sets the unique identifier of the attribute.
        /// </summary>
        public virtual Guid AttributeId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the object associated with the attribute.
        /// </summary>
        public virtual Guid ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the attribute's name in the string table.
        /// </summary>
        public virtual int AttributeKey { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the attribute's name in the string table.
        /// </summary>
        public virtual StringTableEntryData AttributeKeyEntry { get; set; }

        /// <summary>
        /// Gets or sets the name of the attribute.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the text value of the attribute.
        /// </summary>
        public virtual string TextValue { get; set; }

        /// <summary>
        /// Gets or sets the XML value of the attribute.
        /// </summary>
        public virtual string XmlValue { get; set; }

        /// <summary>
        /// Gets or sets the GUID value of the attribute.
        /// </summary>
        public virtual Guid? GuidRefValue { get; set; }
    }
}