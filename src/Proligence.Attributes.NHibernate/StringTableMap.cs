﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringTableMap.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.NHibernate
{
    using System.Diagnostics.CodeAnalysis;
    using FluentNHibernate.Mapping;

    /// <summary>
    /// Defines NHibernate mappings for the string table.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class StringTableMap : ClassMap<StringTableEntryData>
    {
        /// <summary>
        /// The name of the table which stores the mapped entity.
        /// </summary>
        public const string TableName = "dbo.StringTable";

        /// <summary>
        /// Initializes a new instance of the <see cref="StringTableMap"/> class.
        /// </summary>
        public StringTableMap()
        {
            this.Table(TableName);
            this.MapIdentityColumn();
            this.MapColumns();
        }

        /// <summary>
        /// Sets up a mapping for the identity column for the table.
        /// </summary>
        protected void MapIdentityColumn()
        {
            this.Id(x => x.StringId);
        }

        /// <summary>
        /// Sets up mappings for standard attribute columns.
        /// </summary>
        protected void MapColumns()
        {
            this.Map(x => x.Value);
        }
    }
}