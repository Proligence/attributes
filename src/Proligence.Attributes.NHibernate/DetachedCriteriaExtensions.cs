﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DetachedCriteriaExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.NHibernate
{
    using System;
    using global::NHibernate;
    using global::NHibernate.Criterion;
    
    /// <summary>
    /// Implements extension methods for the <see cref="DetachedCriteria"/> class.
    /// </summary>
    public static class DetachedCriteriaExtensions
    {
        /// <summary>
        /// Adds an attribute restriction to the specified <see cref="ICriteria"/>.
        /// </summary>
        /// <param name="criteria">The <see cref="ICriteria"/> instance.</param>
        /// <param name="key">The key of the attribute to restrict.</param>
        /// <returns>The result <see cref="ICriteria"/>.</returns>
        public static DetachedCriteria AddAttributeRestriction(this DetachedCriteria criteria, string key)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }

            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            criteria
                .CreateCriteria("Attributes")
                .CreateCriteria("AttributeKeyEntry")
                .Add(Restrictions.Eq("Value", key));

            return criteria;
        }

        /// <summary>
        /// Adds an attribute restriction to the specified <see cref="ICriteria"/>.
        /// </summary>
        /// <param name="criteria">The <see cref="ICriteria"/> instance.</param>
        /// <param name="key">The key of the attribute to restrict.</param>
        /// <param name="textValue">The text value of the attribute to restrict.</param>
        /// <returns>The result <see cref="ICriteria"/>.</returns>
        public static DetachedCriteria AddAttributeRestriction(
            this DetachedCriteria criteria,
            string key,
            string textValue)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }

            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            if (string.IsNullOrEmpty(textValue))
            {
                throw new ArgumentNullException("textValue");
            }

            criteria
                .CreateCriteria("Attributes")
                .Add(Restrictions.Eq("TextValue", textValue))
                .CreateCriteria("AttributeKeyEntry")
                .Add(Restrictions.Eq("Value", key));

            return criteria;
        }

        /// <summary>
        /// Adds an attribute restriction to the specified <see cref="ICriteria"/>.
        /// </summary>
        /// <param name="criteria">The <see cref="ICriteria"/> instance.</param>
        /// <param name="key">The key of the attribute to restrict.</param>
        /// <param name="textValue">The text value of the attribute to restrict.</param>
        /// <param name="refValue">The object reference value of the attribute to restrict.</param>
        /// <returns>The result <see cref="ICriteria"/>.</returns>
        public static DetachedCriteria AddAttributeRestriction(
            this DetachedCriteria criteria, 
            string key, 
            string textValue, 
            Guid refValue)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }

            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            if (string.IsNullOrEmpty(textValue))
            {
                throw new ArgumentNullException("textValue");
            }

            criteria
                .CreateCriteria("Attributes")
                .Add(Restrictions.Eq("TextValue", textValue))
                .Add(Restrictions.Eq("GuidRefValue", refValue))
                .CreateCriteria("AttributeKeyEntry")
                .Add(Restrictions.Eq("Value", key));

            return criteria;
        }

        /// <summary>
        /// Adds an attribute restriction to the specified <see cref="ICriteria"/>.
        /// </summary>
        /// <param name="criteria">The <see cref="ICriteria"/> instance.</param>
        /// <param name="key">The key of the attribute to restrict.</param>
        /// <param name="refValue">The object reference value of the attribute to restrict.</param>
        /// <returns>The result <see cref="ICriteria"/>.</returns>
        public static DetachedCriteria AddAttributeRestriction(
            this DetachedCriteria criteria,
            string key,
            Guid refValue)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }

            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            var attributeCriteria = criteria.GetCriteriaByPath("Attributes") ?? 
                                    criteria.CreateCriteria("Attributes");
            attributeCriteria.Add(Restrictions.Eq("GuidRefValue", refValue));

            var keyCriteria = attributeCriteria.GetCriteriaByPath("AttributeKeyEntry") ??
                              attributeCriteria.CreateCriteria("AttributeKeyEntry");
            keyCriteria.Add(Restrictions.Eq("Value", key));

            return criteria;
        }
    }
}