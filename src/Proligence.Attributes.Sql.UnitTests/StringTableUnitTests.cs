﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringTableUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Sql.UnitTests
{
    using System;
    using NUnit.Framework;

    /// <summary>
    /// Implements unit tests for the <see cref="StringTable"/> class.
    /// </summary>
    [TestFixture]
    public class StringTableUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="StringTable"/> throws an <see cref="ArgumentNullException"/>
        /// when the specified server name is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateStringTableWhenServerNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new StringTable(null, "MyDatabase"));

            Assert.That(exception.ParamName, Is.EqualTo("serverName"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="StringTable"/> throws an <see cref="ArgumentNullException"/>
        /// when the specified database name is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateStringTableWhenDatabaseNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new StringTable("MyServer", null));

            Assert.That(exception.ParamName, Is.EqualTo("databaseName"));
        }

        /// <summary>
        /// Tests if the <see cref="StringTable.GetStringId"/> method throws an <see cref="ArgumentNullException"/>
        /// when the specified string is <c>null</c> or empty.
        /// </summary>
        /// <param name="value">The string to test.</param>
        [TestCase(null)]
        [TestCase("")]
        [Category("Integration")]
        public void GetStringIdWhenStringNullOrEmpty(string value)
        {
            using (var stringTable = new StringTable(SqlHelper.ServerName, SqlHelper.DatabaseName))
            {
                ArgumentNullException exception =
                    Assert.Throws<ArgumentNullException>(() => stringTable.GetStringId(value));

                Assert.That(exception.ParamName, Is.EqualTo("value"));
            }
        }

        /// <summary>
        /// Tests if the <see cref="StringTable.GetStringId"/> method works correctly when the specified string is
        /// present in the string table.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetStringIdWhenStringPresentInStringTable()
        {
            using (var stringTable = new StringTable(SqlHelper.ServerName, SqlHelper.DatabaseName))
            {
                string testString;
                int testStringId = SqlHelper.NewTestString(out testString);

                int stringId = stringTable.GetStringId(testString);

                Assert.That(stringId, Is.EqualTo(testStringId));
            }
        }

        /// <summary>
        /// Tests if the <see cref="StringTable.GetStringId"/> method works correctly when the specified string is not
        /// present in the string table.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetStringIdWhenStringNotPresentInStringTable()
        {
            using (var stringTable = new StringTable(SqlHelper.ServerName, SqlHelper.DatabaseName))
            {
                string testString = Guid.NewGuid().ToString();
                int result = stringTable.GetStringId(testString);

                int actualStringId = SqlHelper.GetExistingStringId(testString);
                Assert.That(result, Is.EqualTo(actualStringId));
            }
        }

        /// <summary>
        /// Tests if the <see cref="StringTable.GetString"/> method works correctly when the specified string is
        /// present in the string table.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetStringWhenStringPresentInStringTable()
        {
            using (var stringTable = new StringTable(SqlHelper.ServerName, SqlHelper.DatabaseName))
            {
                string testString;
                int testStringId = SqlHelper.NewTestString(out testString);

                string str = stringTable.GetString(testStringId);

                Assert.That(str, Is.EqualTo(testString));
            }
        }

        /// <summary>
        /// Tests if the <see cref="StringTable.GetString"/> method works correctly when the specified string is not
        /// present in the string table.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetStringWhenStringNotPresentInStringTable()
        {
            using (var stringTable = new StringTable(SqlHelper.ServerName, SqlHelper.DatabaseName))
            {
                string str = stringTable.GetString(int.MaxValue);

                Assert.That(str, Is.Null);
            }
        }
    }
}