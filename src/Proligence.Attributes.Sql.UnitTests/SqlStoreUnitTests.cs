﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlStoreUnitTests.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Sql.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements unit tests for the <see cref="SqlStore"/> class.
    /// </summary>
    [TestFixture]
    public class SqlStoreUnitTests
    {
        /// <summary>
        /// Tests if the constructor of the <see cref="SqlStore"/> throws an <see cref="ArgumentNullException"/> when
        /// the specified server name is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateSqlStoreWhenServerNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new SqlStore(null, "MyDatabase"));

            Assert.That(exception.ParamName, Is.EqualTo("serverName"));
        }

        /// <summary>
        /// Tests if the constructor of the <see cref="SqlStore"/> throws an <see cref="ArgumentNullException"/> when
        /// the specified database name is <c>null</c>.
        /// </summary>
        [Test]
        public void CreateSqlStoreWhenDatabaseNameNull()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => new SqlStore("MyServer", null));

            Assert.That(exception.ParamName, Is.EqualTo("databaseName"));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetAttributes"/> method works properly when the specified object does not
        /// have any attribute.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetAttributesWhenNoAttributes()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            IEnumerable<AttributeData> attributes = store.GetAttributes(Guid.NewGuid());

            Assert.That(attributes, Is.Empty);
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetAttributes"/> method works properly when the specified object has
        /// attributes.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetAttributesWhenAttributesExist()
        {
            Guid objectId = Guid.NewGuid();
            Guid[] refs = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };
            Guid attr1Id = SqlHelper.CreateAttribute(objectId, "MyKey", "MyValue1", "<Root>1</Root>", refs[0]);
            Guid attr2Id = SqlHelper.CreateAttribute(objectId, "MyKey", "MyValue2", "<Root>2</Root>", refs[1]);
            Guid attr3Id = SqlHelper.CreateAttribute(objectId, "MyKey2", "MyValue3", "<Root>3</Root>", refs[2]);

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            IEnumerable<AttributeData> attributes = store.GetAttributes(objectId);

            AttributeData[] attrs = attributes.ToArray();
            Assert.That(attrs.Count(), Is.EqualTo(3));

            AttributeData attr1 = attrs.Single(x => x.TextValue == "MyValue1");
            Assert.That(attr1.AttributeId, Is.EqualTo(attr1Id));
            Assert.That(attr1.ObjectId, Is.EqualTo(objectId));
            Assert.That(attr1.Key, Is.EqualTo("MyKey"));
            Assert.That(attr1.TextValue, Is.EqualTo("MyValue1"));
            Assert.That(attr1.XmlValue, Is.EqualTo("<Root>1</Root>"));
            Assert.That(attr1.GuidRefValue, Is.EqualTo(refs[0]));

            AttributeData attr2 = attrs.Single(x => x.TextValue == "MyValue2");
            Assert.That(attr2.AttributeId, Is.EqualTo(attr2Id));
            Assert.That(attr2.ObjectId, Is.EqualTo(objectId));
            Assert.That(attr2.Key, Is.EqualTo("MyKey"));
            Assert.That(attr2.TextValue, Is.EqualTo("MyValue2"));
            Assert.That(attr2.XmlValue, Is.EqualTo("<Root>2</Root>"));
            Assert.That(attr2.GuidRefValue, Is.EqualTo(refs[1]));

            AttributeData attr3 = attrs.Single(x => x.TextValue == "MyValue3");
            Assert.That(attr3.AttributeId, Is.EqualTo(attr3Id));
            Assert.That(attr3.ObjectId, Is.EqualTo(objectId));
            Assert.That(attr3.Key, Is.EqualTo("MyKey2"));
            Assert.That(attr3.TextValue, Is.EqualTo("MyValue3"));
            Assert.That(attr3.XmlValue, Is.EqualTo("<Root>3</Root>"));
            Assert.That(attr3.GuidRefValue, Is.EqualTo(refs[2]));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetAttribute"/> method works properly when there is no attribute with
        /// the specified identifier.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetAttributeWhenAttributeDoesNotExist()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);
            
            AttributeData attribute = store.GetAttribute(Guid.NewGuid());

            Assert.That(attribute, Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetAttribute"/> method works properly when an attribute with the
        /// specified identifier exists in the database.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetAttributeWhenAttributeExists()
        {
            Guid objectId = Guid.NewGuid();
            Guid refValue = Guid.NewGuid();
            Guid attributeId = SqlHelper.CreateAttribute(objectId, "MyKey", "MyValue", "<Root>Xml</Root>", refValue);

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            AttributeData attribute = store.GetAttribute(attributeId);

            Assert.That(attribute.AttributeId, Is.EqualTo(attributeId));
            Assert.That(attribute.ObjectId, Is.EqualTo(objectId));
            Assert.That(attribute.Key, Is.EqualTo("MyKey"));
            Assert.That(attribute.TextValue, Is.EqualTo("MyValue"));
            Assert.That(attribute.XmlValue, Is.EqualTo("<Root>Xml</Root>"));
            Assert.That(attribute.GuidRefValue, Is.EqualTo(refValue));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetAttribute"/> method works properly when an attribute with the
        /// specified identifier has attribute members.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetAttributeWhenAttributeHasDataMembers()
        {
            Guid objectId = Guid.NewGuid();
            Guid refValue = Guid.NewGuid();
            Guid attributeId = SqlHelper.CreateAttribute(
                objectId,
                "MyKey",
                "MyValue",
                "<Root>Xml</Root>",
                refValue,
                new KeyValuePair<string, object>("StringProperty", "Test"),
                new KeyValuePair<string, object>("Int", 7),
                new KeyValuePair<string, object>("BooleanProperty", true));

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            AttributeData attribute = store.GetAttribute(attributeId);

            Assert.That(attribute.DataMembers["StringProperty"], Is.EqualTo("Test"));
            Assert.That(attribute.DataMembers["Int"], Is.EqualTo(7));
            Assert.That(attribute.DataMembers["BooleanProperty"], Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.SaveAttributes"/> method throws an <see cref="ArgumentNullException"/>
        /// when the specified sequence of attributes is <c>null</c>.
        /// </summary>
        [Test]
        public void SaveAttributesWhenAttributesNull()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => store.SaveAttributes(null, false));

            Assert.That(exception.ParamName, Is.EqualTo("attributes"));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.SaveAttributes"/> method works correctly creates new attributes when the
        /// specified attributes do not exist in the database.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void SaveAttributesWhenAttributesDoNotExist()
        {
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            AttributeData[] attributes = new[]
            {
                new AttributeData
                {
                    Key = "MyKey1",
                    ObjectId = objectId1,
                    TextValue = "MyValue1",
                    XmlValue = "<Root>Xml1</Root>",
                    GuidRefValue = refValue1
                },
                new AttributeData
                {
                    Key = "MyKey2",
                    ObjectId = objectId2,
                    TextValue = "MyValue2",
                    XmlValue = "<Root>Xml2</Root>",
                    GuidRefValue = refValue2
                }
            };

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            store.SaveAttributes(attributes, false);

            Assert.That(attributes[0].AttributeId, Is.Not.EqualTo(Guid.Empty));
            Assert.That(attributes[0].ObjectId, Is.EqualTo(objectId1));
            Assert.That(attributes[0].Key, Is.EqualTo("MyKey1"));
            Assert.That(attributes[0].TextValue, Is.EqualTo("MyValue1"));
            Assert.That(attributes[0].XmlValue, Is.EqualTo("<Root>Xml1</Root>"));
            Assert.That(attributes[0].GuidRefValue, Is.EqualTo(refValue1));

            Assert.That(attributes[1].AttributeId, Is.Not.EqualTo(Guid.Empty));
            Assert.That(attributes[1].ObjectId, Is.EqualTo(objectId2));
            Assert.That(attributes[1].Key, Is.EqualTo("MyKey2"));
            Assert.That(attributes[1].TextValue, Is.EqualTo("MyValue2"));
            Assert.That(attributes[1].XmlValue, Is.EqualTo("<Root>Xml2</Root>"));
            Assert.That(attributes[1].GuidRefValue, Is.EqualTo(refValue2));
            
            object[] attr1 = SqlHelper.GetAttribute(objectId1, "MyKey1");
            Assert.That(attr1[0], Is.EqualTo(attributes[0].AttributeId));
            Assert.That(attr1[1], Is.EqualTo(objectId1));
            Assert.That(attr1[2], Is.EqualTo("MyKey1"));
            Assert.That(attr1[3], Is.EqualTo("MyValue1"));
            Assert.That(attr1[4], Is.EqualTo("<Root>Xml1</Root>"));
            Assert.That(attr1[5], Is.EqualTo(refValue1));

            object[] attr2 = SqlHelper.GetAttribute(objectId2, "MyKey2");
            Assert.That(attr2[0], Is.EqualTo(attributes[1].AttributeId));
            Assert.That(attr2[1], Is.EqualTo(objectId2));
            Assert.That(attr2[2], Is.EqualTo("MyKey2"));
            Assert.That(attr2[3], Is.EqualTo("MyValue2"));
            Assert.That(attr2[4], Is.EqualTo("<Root>Xml2</Root>"));
            Assert.That(attr2[5], Is.EqualTo(refValue2));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.SaveAttributes"/> method works correctly updates attributes when the
        /// specified attributes exist in the database.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void SaveAttributesWhenAttributesExist()
        {
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            Guid attr1Id = SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", "<Root>1</Root>", refValue1);
            Guid attr2Id = SqlHelper.CreateAttribute(objectId2, "MyKey2", "MyValue2", "<Root>2</Root>", refValue2);

            objectId1 = Guid.NewGuid();
            objectId2 = Guid.NewGuid();
            refValue1 = Guid.NewGuid();
            refValue2 = Guid.NewGuid();

            AttributeData[] attributes = new[]
            {
                new AttributeData
                {
                    AttributeId = attr1Id,
                    Key = "MyNewKey1",
                    ObjectId = objectId1,
                    TextValue = "MyNewValue1",
                    XmlValue = "<Root>NewXml1</Root>",
                    GuidRefValue = refValue1
                },
                new AttributeData
                {
                    AttributeId = attr2Id,
                    Key = "MyNewKey2",
                    ObjectId = objectId2,
                    TextValue = "MyNewValue2",
                    XmlValue = "<Root>NewXml2</Root>",
                    GuidRefValue = refValue2
                }
            };

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            store.SaveAttributes(attributes, false);

            Assert.That(attributes[0].AttributeId, Is.EqualTo(attr1Id));
            Assert.That(attributes[0].ObjectId, Is.EqualTo(objectId1));
            Assert.That(attributes[0].Key, Is.EqualTo("MyNewKey1"));
            Assert.That(attributes[0].TextValue, Is.EqualTo("MyNewValue1"));
            Assert.That(attributes[0].XmlValue, Is.EqualTo("<Root>NewXml1</Root>"));
            Assert.That(attributes[0].GuidRefValue, Is.EqualTo(refValue1));

            Assert.That(attributes[1].AttributeId, Is.EqualTo(attr2Id));
            Assert.That(attributes[1].ObjectId, Is.EqualTo(objectId2));
            Assert.That(attributes[1].Key, Is.EqualTo("MyNewKey2"));
            Assert.That(attributes[1].TextValue, Is.EqualTo("MyNewValue2"));
            Assert.That(attributes[1].XmlValue, Is.EqualTo("<Root>NewXml2</Root>"));
            Assert.That(attributes[1].GuidRefValue, Is.EqualTo(refValue2));

            object[] attr1 = SqlHelper.GetAttribute(objectId1, "MyNewKey1");
            Assert.That(attr1[0], Is.EqualTo(attr1Id));
            Assert.That(attr1[1], Is.EqualTo(objectId1));
            Assert.That(attr1[2], Is.EqualTo("MyNewKey1"));
            Assert.That(attr1[3], Is.EqualTo("MyNewValue1"));
            Assert.That(attr1[4], Is.EqualTo("<Root>NewXml1</Root>"));
            Assert.That(attr1[5], Is.EqualTo(refValue1));

            object[] attr2 = SqlHelper.GetAttribute(objectId2, "MyNewKey2");
            Assert.That(attr2[0], Is.EqualTo(attr2Id));
            Assert.That(attr2[1], Is.EqualTo(objectId2));
            Assert.That(attr2[2], Is.EqualTo("MyNewKey2"));
            Assert.That(attr2[3], Is.EqualTo("MyNewValue2"));
            Assert.That(attr2[4], Is.EqualTo("<Root>NewXml2</Root>"));
            Assert.That(attr2[5], Is.EqualTo(refValue2));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.SaveAttributes"/> method works correctly creates an attribute without any
        /// optional data.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void SaveAttributeWhenMinimumAttributeDataSpecified()
        {
            Guid objectId = Guid.NewGuid();
            AttributeData[] attributes = new[] { new AttributeData { Key = "MyKey", ObjectId = objectId } };

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            store.SaveAttributes(attributes, false);

            Assert.That(attributes[0].AttributeId, Is.Not.EqualTo(Guid.Empty));
            Assert.That(attributes[0].ObjectId, Is.EqualTo(objectId));
            Assert.That(attributes[0].Key, Is.EqualTo("MyKey"));
            Assert.That(attributes[0].TextValue, Is.Null);
            Assert.That(attributes[0].XmlValue, Is.Null);
            Assert.That(attributes[0].GuidRefValue, Is.Null);

            object[] attr1 = SqlHelper.GetAttribute(objectId, "MyKey");
            Assert.That(attr1[0], Is.EqualTo(attributes[0].AttributeId));
            Assert.That(attr1[1], Is.EqualTo(objectId));
            Assert.That(attr1[2], Is.EqualTo("MyKey"));
            Assert.That(attr1[3], Is.Null);
            Assert.That(attr1[4], Is.Null);
            Assert.That(attr1[5], Is.Null);
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.SaveAttributes"/> method correctly creates attributes with attribute
        /// data members.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void SaveAttributeWhenAttributeHasDataMembers()
        {
            Guid objectId = Guid.NewGuid();
            
            var attribute = new AttributeData { Key = "MyKey", ObjectId = objectId };
            attribute.DataMembers["StringProperty"] = "Test";
            attribute.DataMembers["Int"] = 7;
            attribute.DataMembers["BooleanProperty"] = true;

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            store.SaveAttributes(new[] { attribute }, false);

            object[] attr1 = SqlHelper.GetAttribute(objectId, "MyKey");
            Assert.That(attr1[6], Is.EqualTo("Test"));
            Assert.That(attr1[7], Is.EqualTo(7));
            Assert.That(attr1[8], Is.EqualTo(true));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.SaveAttributes"/> method correctly overwrites attributes with the same
        /// key when <c>overwriteExisting</c> parameter is set to <c>true</c>.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void SaveAttributeWhenOverwriteTrueAndAttributeWithSameKeyExists()
        {
            Guid objectId = Guid.NewGuid();
            Guid refValue = Guid.NewGuid();
            Guid attrId = SqlHelper.CreateAttribute(objectId, "MyKey1", "MyValue1", "<Root>1</Root>", refValue);

            refValue = Guid.NewGuid();
            
            AttributeData[] attributes = 
            {
                new AttributeData
                {
                    Key = "MyKey1",
                    ObjectId = objectId,
                    TextValue = "MyNewValue1",
                    XmlValue = "<Root>NewXml1</Root>",
                    GuidRefValue = refValue
                }
            };

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            store.SaveAttributes(attributes, true);

            Assert.That(attributes[0].AttributeId, Is.EqualTo(attrId));
            Assert.That(attributes[0].ObjectId, Is.EqualTo(objectId));
            Assert.That(attributes[0].Key, Is.EqualTo("MyKey1"));
            Assert.That(attributes[0].TextValue, Is.EqualTo("MyNewValue1"));
            Assert.That(attributes[0].XmlValue, Is.EqualTo("<Root>NewXml1</Root>"));
            Assert.That(attributes[0].GuidRefValue, Is.EqualTo(refValue));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.RemoveAttributes"/> method throws an <see cref="ArgumentNullException"/>
        /// when the specified sequence of attributes is <c>null</c>.
        /// </summary>
        [Test]
        public void RemoveAttributesWhenAttributesNull()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(
                () => store.RemoveAttributes(null));

            Assert.That(exception.ParamName, Is.EqualTo("attributeIds"));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.RemoveAttributes"/> method works correctly when the specified attributes
        /// exist in the database.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void RemoveAttributesWhenAttributesExist()
        {
            Guid attr1Id = SqlHelper.CreateAttribute(
                Guid.NewGuid(), "MyKey1", "MyValue1", "<Root>1</Root>", Guid.NewGuid());
            
            Guid attr2Id = SqlHelper.CreateAttribute(
                Guid.NewGuid(), "MyKey2", "MyValue2", "<Root>2</Root>", Guid.NewGuid());

            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            store.RemoveAttributes(new[] { attr1Id, attr2Id });

            object[] attr1 = SqlHelper.GetAttribute(attr1Id);
            Assert.IsNull(attr1);
            
            object[] attr2 = SqlHelper.GetAttribute(attr2Id);
            Assert.IsNull(attr2);
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.RemoveAttributes"/> method works correctly when the specified attributes
        /// do not exist in the database.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void RemoveAttributesWhenAttributesDoNotExist()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Assert.DoesNotThrow(
                () => store.RemoveAttributes(new[] { Guid.NewGuid() }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetObjects"/> method throws an <see cref="ArgumentException"/> when all
        /// the specified parameters are <c>null</c> or empty.
        /// </summary>
        /// <param name="key">The tested key value.</param>
        [TestCase(null)]
        [TestCase("")]
        public void GetObjectsWhenAllParamsNull(string key)
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => store.GetObjects(key));

            Assert.That(exception.Message, Is.StringContaining("At least one non-null parameter must be specified."));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetObjects"/> method works correctly when a valid attribute key (name)
        /// is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetObjectsWhenKeySpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue1");
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2");

            IEnumerable<Guid> objectIds = store.GetObjects(key);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId2 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetObjects"/> method works correctly when a valid attribute key (name)
        /// and value is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetObjectsWhenKeyAndValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue");
            SqlHelper.CreateAttribute(objectId2, key, "MyValue");
            SqlHelper.CreateAttribute(objectId3, key, "MyValue2");

            IEnumerable<Guid> objectIds = store.GetObjects(key, "MyValue");

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId2 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetObjects"/> method works correctly when a valid attribute key (name),
        /// value and GUID reference value is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetObjectsWhenKeyValueAndGuidRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, key, "MyValue", null, refValue1);

            IEnumerable<Guid> objectIds = store.GetObjects(key, "MyValue", refValue1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetObjects"/> method works correctly when a valid attribute key (name)
        /// and GUID reference value is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetObjectsWhenKeyAndGuidRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, key, "MyValue1", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, key, "MyValue3", null, refValue1);

            IEnumerable<Guid> objectIds = store.GetObjects(key, null, refValue1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetObjects"/> method works correctly when only an attribute text value
        /// is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetObjectsWhenValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string value1 = Guid.NewGuid().ToString();
            string value2 = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();
            Guid refValue3 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, "MyKey1", value1, null, refValue1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", value2, null, refValue2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", value1, null, refValue3);

            IEnumerable<Guid> objectIds = store.GetObjects(null, value1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetObjects"/> method works correctly when only an attribute GUID
        /// reference value is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetObjectsWhenRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", "MyValue3", null, refValue1);

            IEnumerable<Guid> objectIds = store.GetObjects(null, null, refValue1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetObjects"/> method works correctly when an attribute text value and
        /// GUID reference values is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetObjectsWhenValueAndRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            Guid objectId5 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", "MyValue1", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", "MyValue2", null, refValue1);
            SqlHelper.CreateAttribute(objectId4, "MyKey4", "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId5, "MyKey5", "MyValue1", null, refValue2);

            IEnumerable<Guid> objectIds = store.GetObjects(null, "MyValue1", refValue2);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId2, objectId5 }));
        }

        [TestCase(null)]
        [TestCase("")]
        public void GetObjectsBySetWhenKeyNullAndTextValuesNull(string key)
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            var exception = Assert.Throws<ArgumentException>(
                () => store.GetObjectsBySet(key, null));

            Assert.That(exception.Message, Is.StringContaining("At least one non-null parameter must be specified."));
        }

        [TestCase(null)]
        [TestCase("")]
        public void GetObjectsBySetWhenKeyNullTextValuesEmpty(string key)
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            var exception = Assert.Throws<ArgumentException>(
                () => store.GetObjectsBySet(key, new string[0]));

            Assert.That(exception.Message, Is.StringContaining("At least one non-null parameter must be specified."));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenOnlyKeySpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue1");
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2");

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(key, null);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId2 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenKeyAndZeroValuesSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue1");
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2");

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(key, new string[0]);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId2 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenKeyAndSingleValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue");
            SqlHelper.CreateAttribute(objectId2, key, "MyValue");
            SqlHelper.CreateAttribute(objectId3, key, "MyValue2");

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(key, new[] { "MyValue" });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId2 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenKeyAndMultipleValuesSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue");
            SqlHelper.CreateAttribute(objectId2, key, "MyValue");
            SqlHelper.CreateAttribute(objectId3, key, "MyValue2");
            SqlHelper.CreateAttribute(objectId4, key, "MyValue3");

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(key, new[] { "MyValue", "MyValue2" });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId2, objectId3 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenKeyValueAndGuidRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            Guid objectId5 = Guid.NewGuid();
            Guid objectId6 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();
            
            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, key, "MyValue", null, refValue1);
            SqlHelper.CreateAttribute(objectId4, key, "MyValue2", null, refValue1);
            SqlHelper.CreateAttribute(objectId5, key, "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId6, key, "MyValue2", null, refValue1);

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(key, new[] { "MyValue", "MyValue2" }, refValue1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3, objectId4, objectId6 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenKeyAndGuidRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, key, "MyValue1", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, key, "MyValue3", null, refValue1);

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(key, null, refValue1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenKeyEmptyValuesAndGuidRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, key, "MyValue1", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, key, "MyValue3", null, refValue1);

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(key, new string[0], refValue1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenOnlySingleValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string value1 = Guid.NewGuid().ToString();
            string value2 = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();
            Guid refValue3 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, "MyKey1", value1, null, refValue1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", value2, null, refValue2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", value1, null, refValue3);

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(null, new[] { value1 });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenOnlyMultipleValuesSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string value1 = Guid.NewGuid().ToString();
            string value2 = Guid.NewGuid().ToString();
            string value3 = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();
            Guid refValue3 = Guid.NewGuid();
            Guid refValue4 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, "MyKey1", value1, null, refValue1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", value2, null, refValue2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", value1, null, refValue3);
            SqlHelper.CreateAttribute(objectId4, "MyKey4", value3, null, refValue4);

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(null, new[] { value1, value2 });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId2, objectId3 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenOnlyRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", "MyValue3", null, refValue1);

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(null, null, refValue1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId1, objectId3 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenSingleValueAndRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            Guid objectId5 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", "MyValue1", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", "MyValue2", null, refValue1);
            SqlHelper.CreateAttribute(objectId4, "MyKey4", "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId5, "MyKey5", "MyValue1", null, refValue2);

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(null, new[] { "MyValue1" }, refValue2);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId2, objectId5 }));
        }

        [Test, Category("Integration")]
        public void GetObjectsBySetWhenMultipleValuesAndRefValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            Guid objectId5 = Guid.NewGuid();
            Guid objectId6 = Guid.NewGuid();
            Guid refValue1 = Guid.NewGuid();
            Guid refValue2 = Guid.NewGuid();

            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refValue1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", "MyValue1", null, refValue2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", "MyValue2", null, refValue1);
            SqlHelper.CreateAttribute(objectId4, "MyKey4", "MyValue2", null, refValue2);
            SqlHelper.CreateAttribute(objectId5, "MyKey5", "MyValue1", null, refValue2);
            SqlHelper.CreateAttribute(objectId6, "MyKey6", "MyValue3", null, refValue2);

            IEnumerable<Guid> objectIds = store.GetObjectsBySet(null, new[] { "MyValue1", "MyValue3" }, refValue2);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { objectId2, objectId5, objectId6 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method throws an <see cref="ArgumentException"/>
        /// when all the specified arguments are <c>null</c> or empty.
        /// </summary>
        /// <param name="key">The tested key value.</param>
        [TestCase(null)]
        [TestCase("")]
        public void GetReferencedObjectsWhenAllParamsNull(string key)
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            ArgumentException exception = Assert.Throws<ArgumentException>(
                () => store.GetReferencedObjects(key));

            Assert.That(exception.Message, Is.StringContaining("At least one non-null parameter must be specified."));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method works correctly when a valid attribute
        /// key (name) is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetReferencedObjectsWhenKeySpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue1", null, refId1);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue2", null, refId2);

            IEnumerable<Guid> objectIds = store.GetReferencedObjects(key);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId2 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method works correctly when a valid attribute
        /// key (name) and value is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetReferencedObjectsWhenKeyAndValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue", null, refId3);

            IEnumerable<Guid> objectIds = store.GetReferencedObjects(key, "MyValue");

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId3 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method works correctly when a valid attribute
        /// key (name), value and object identifier is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetReferencedObjectsWhenKeyValueAndObjectIdSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, key, "MyValue2", null, refId3);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue", null, refId4);

            IEnumerable<Guid> objectIds = store.GetReferencedObjects(key, "MyValue", objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method works correctly when a valid attribute
        /// key (name) and object identifier is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetReferencedObjectsWhenKeyAndObjectIdSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, key, "MyValue2", null, refId3);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue", null, refId4);

            IEnumerable<Guid> objectIds = store.GetReferencedObjects(key, null, objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId3 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method works correctly with attributes which do
        /// not have a GUID reference value.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetReferencedObjectsWhenGuidRefValueNull()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue");

            IEnumerable<Guid> objectIds = store.GetReferencedObjects(key, "MyValue");

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method works correctly when only the attribute
        /// text value is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetReferencedObjectsWhenValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string value1 = Guid.NewGuid().ToString();
            string value2 = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, "MyKey1", value1, null, refId1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", value2, null, refId2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", value2);
            SqlHelper.CreateAttribute(objectId4, "MyKey4", value2, null, refId3);

            IEnumerable<Guid> objectIds = store.GetReferencedObjects(null, value2);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId2, refId3 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method works correctly when only the object
        /// identifier is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetReferencedObjectsWhenObjectIdSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refId1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, "MyKey3", "MyValue2");
            SqlHelper.CreateAttribute(objectId1, "MyKey4", "MyValue2", null, refId3);

            IEnumerable<Guid> objectIds = store.GetReferencedObjects(null, null, objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId3 }));
        }

        /// <summary>
        /// Tests if the <see cref="SqlStore.GetReferencedObjects"/> method works correctly when the object identifier
        /// and attribute text value is specified.
        /// </summary>
        [Test]
        [Category("Integration")]
        public void GetReferencedObjectsWhenObjectIdAndValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            Guid refId5 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refId1);
            SqlHelper.CreateAttribute(objectId1, "MyKey2", "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, "MyKey3", "MyValue1");
            SqlHelper.CreateAttribute(objectId2, "MyKey4", "MyValue1", null, refId3);
            SqlHelper.CreateAttribute(objectId2, "MyKey5", "MyValue2", null, refId4);
            SqlHelper.CreateAttribute(objectId1, "MyKey6", "MyValue1", null, refId5);

            IEnumerable<Guid> objectIds = store.GetReferencedObjects(null, "MyValue1", objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId5 }));
        }

        [TestCase(null)]
        [TestCase("")]
        public void GetReferencedObjectsBySetWhenKeyNullAndValuesNull(string key)
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            var exception = Assert.Throws<ArgumentException>(
                () => store.GetReferencedObjectsBySet(key, null));

            Assert.That(exception.Message, Is.StringContaining("At least one non-null parameter must be specified."));
        }

        [TestCase(null)]
        [TestCase("")]
        public void GetReferencedObjectsBySetWhenKeyNullAndValuesEmpty(string key)
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            var exception = Assert.Throws<ArgumentException>(
                () => store.GetReferencedObjectsBySet(key, new string[0]));

            Assert.That(exception.Message, Is.StringContaining("At least one non-null parameter must be specified."));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenOnlyKeySpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue1", null, refId1);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue2", null, refId2);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(key, null);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId2 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenKeyAndEmptyValuesSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue1", null, refId1);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue2", null, refId2);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(key, new string[0]);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId2 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenKeyAndSingleValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue", null, refId3);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(key, new[] { "MyValue" });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId3 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenKeyAndMultipleValuesSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue", null, refId3);
            SqlHelper.CreateAttribute(Guid.NewGuid(), key, "MyValue3", null, refId4);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(key, new[] { "MyValue", "MyValue2" });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId2, refId3 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenKeySingleValueAndObjectIdSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, key, "MyValue2", null, refId3);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue", null, refId4);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(key, new[] { "MyValue" }, objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenKeyMultipleValuesAndObjectIdSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            Guid refId5 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, key, "MyValue2", null, refId3);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue", null, refId4);
            SqlHelper.CreateAttribute(objectId1, key, "MyValue3", null, refId5);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue3", null, refId4);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(
                key,
                new[] { "MyValue", "MyValue3" },
                objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId5 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenKeyAndObjectIdSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, key, "MyValue2", null, refId3);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue", null, refId4);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(key, null, objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId3 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenGuidRefValueNull()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string key = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, key, "MyValue", null, refId1);
            SqlHelper.CreateAttribute(objectId2, key, "MyValue");

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(key, new[] { "MyValue" });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenOnlySingleValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string value1 = Guid.NewGuid().ToString();
            string value2 = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, "MyKey1", value1, null, refId1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", value2, null, refId2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", value2);
            SqlHelper.CreateAttribute(objectId4, "MyKey4", value2, null, refId3);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(null, new[] { value2 });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId2, refId3 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenOnlyMultipleValuesSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            string value1 = Guid.NewGuid().ToString();
            string value2 = Guid.NewGuid().ToString();
            string value3 = Guid.NewGuid().ToString();
            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid objectId3 = Guid.NewGuid();
            Guid objectId4 = Guid.NewGuid();
            Guid objectId5 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, "MyKey1", value1, null, refId1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", value2, null, refId2);
            SqlHelper.CreateAttribute(objectId3, "MyKey3", value2);
            SqlHelper.CreateAttribute(objectId4, "MyKey4", value2, null, refId3);
            SqlHelper.CreateAttribute(objectId5, "MyKey5", value3, null, refId4);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(null, new[] { value2, value3 });

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId2, refId3, refId4 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenOnlyObjectIdSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refId1);
            SqlHelper.CreateAttribute(objectId2, "MyKey2", "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, "MyKey3", "MyValue2");
            SqlHelper.CreateAttribute(objectId1, "MyKey4", "MyValue2", null, refId3);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(null, null, objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId3 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenObjectIdAndSingleValueSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            Guid refId5 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refId1);
            SqlHelper.CreateAttribute(objectId1, "MyKey2", "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, "MyKey3", "MyValue1");
            SqlHelper.CreateAttribute(objectId2, "MyKey4", "MyValue1", null, refId3);
            SqlHelper.CreateAttribute(objectId2, "MyKey5", "MyValue2", null, refId4);
            SqlHelper.CreateAttribute(objectId1, "MyKey6", "MyValue1", null, refId5);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(null, new[] { "MyValue1" }, objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId5 }));
        }

        [Test, Category("Integration")]
        public void GetReferencedObjectsBySetWhenObjectIdAndMultipleValuesSpecified()
        {
            var store = new SqlStore(SqlHelper.ServerName, SqlHelper.DatabaseName);

            Guid objectId1 = Guid.NewGuid();
            Guid objectId2 = Guid.NewGuid();
            Guid refId1 = Guid.NewGuid();
            Guid refId2 = Guid.NewGuid();
            Guid refId3 = Guid.NewGuid();
            Guid refId4 = Guid.NewGuid();
            Guid refId5 = Guid.NewGuid();
            Guid refId6 = Guid.NewGuid();
            SqlHelper.CreateAttribute(objectId1, "MyKey1", "MyValue1", null, refId1);
            SqlHelper.CreateAttribute(objectId1, "MyKey2", "MyValue2", null, refId2);
            SqlHelper.CreateAttribute(objectId1, "MyKey3", "MyValue1");
            SqlHelper.CreateAttribute(objectId2, "MyKey4", "MyValue1", null, refId3);
            SqlHelper.CreateAttribute(objectId2, "MyKey5", "MyValue2", null, refId4);
            SqlHelper.CreateAttribute(objectId1, "MyKey6", "MyValue1", null, refId5);
            SqlHelper.CreateAttribute(objectId1, "MyKey7", "MyValue4", null, refId6);

            IEnumerable<Guid> objectIds = store.GetReferencedObjectsBySet(
                null,
                new[] { "MyValue1", "MyValue4" },
                objectId1);

            Assert.That(objectIds.ToArray(), Is.EquivalentTo(new[] { refId1, refId5, refId6 }));
        }
    }
}