﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlHelper.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Sql.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Provides helper methods for executing SQL queries on the integration server.
    /// </summary>
    internal static class SqlHelper
    {
        /// <summary>
        /// The name of the server which provides the integration test instance of SQL Server.
        /// </summary>
        public const string ServerName = "localhost";

        /// <summary>
        /// The name of the database which will be used for integration tests.
        /// </summary>
        public const string DatabaseName = "Attributes_INT";

        /// <summary>
        /// Creates a connection to the integration tests SQL database.
        /// </summary>
        /// <returns>The created connection string.</returns>
        public static SqlConnection CreateConnection()
        {
            return new SqlConnection(
                "Data Source=" + ServerName + 
                ";Database=" + DatabaseName + 
                ";Integrated Security=SSPI");
        }

        /// <summary>
        /// Executes the specified SQL query and returns the single value returned by the SQL server.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <param name="parameters">The query parameters.</param>
        /// <returns>The value returned by SQL server or <c>null</c> if no results were returned.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities",
            Justification = "Unit test.")]
        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times",
            Justification = "It's OK here.")]
        public static object ExecuteQuery(string query, params SqlParameter[] parameters)
        {
            using (SqlConnection connection = CreateConnection())
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.Parameters.AddRange(parameters);
                    connection.Open();

                    return command.ExecuteScalar();
                }
            }
        }

        /// <summary>
        /// Gets a single row from the database.
        /// </summary>
        /// <param name="columns">The columns to select.</param>
        /// <param name="tableName">The name of the table.</param>
        /// <param name="where">The 'where' clause which filters the row to get.</param>
        /// <param name="parameters">The parameter for the 'where' clause.</param>
        /// <returns>An array of values from the specified row.</returns>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities",
            Justification = "Unit test.")]
        public static object[] GetTableRow(
            string columns,
            string tableName,
            string where,
            params SqlParameter[] parameters)
        {
            using (SqlConnection connection = CreateConnection())
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT " + columns + " FROM " + tableName + " WHERE " + where;
                    command.Parameters.AddRange(parameters);
                    connection.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var results = new List<object>();

                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (reader.IsDBNull(i))
                                {
                                    results.Add(null);
                                }
                                else
                                {
                                    results.Add(reader.GetValue(i));
                                }
                            }

                            return results.ToArray();
                        }
                    }

                    return null;
                }
            }
        }

        /// <summary>
        /// Creates a new, unique string in the string table.
        /// </summary>
        /// <param name="str">The created string value.</param>
        /// <returns>The created string identifier.</returns>
        public static int NewTestString(out string str)
        {
            str = Guid.NewGuid().ToString();

            return (int)ExecuteQuery(
                "INSERT INTO dbo.StringTable (Value) VALUES (@Value);" +
                "SELECT StringId FROM dbo.StringTable WHERE Value = @Value",
                new SqlParameter("@Value", str));
        }

        /// <summary>
        /// Gets the string table identifier of the specified string.
        /// </summary>
        /// <param name="str">The string to get identifier for.</param>
        /// <returns>The identifier of the specified string.</returns>
        public static int GetExistingStringId(string str)
        {
            return (int)ExecuteQuery(
                "SELECT StringId FROM dbo.StringTable WHERE Value = @Value",
                new SqlParameter("@Value", str));
        }

        /// <summary>
        /// Gets the string table identifier of the specified string and creates the string if it does not exist in
        /// the string table.
        /// </summary>
        /// <param name="str">The string to get identifier for.</param>
        /// <returns>The identifier of the specified string.</returns>
        public static int GetStringId(string str)
        {
            object id = ExecuteQuery(
                "SELECT StringId FROM dbo.StringTable WHERE Value = @Value",
                new SqlParameter("@Value", str));

            if (id == null)
            {
                return (int)ExecuteQuery(
                    "INSERT INTO dbo.StringTable (Value) VALUES (@Value);" + 
                    "SELECT StringId FROM dbo.StringTable WHERE Value = @Value",
                    new SqlParameter("@Value", str));
            }

            return (int)id;
        }

        /// <summary>
        /// Creates an attribute in the test SQL database.
        /// </summary>
        /// <param name="objectId">The identifier of the attribute's object.</param>
        /// <param name="key">The attribute's key (name).</param>
        /// <param name="value">The attribute's text value.</param>
        /// <param name="xml">The attribute's XML value.</param>
        /// <param name="refValue">The attributes GUID reference value.</param>
        /// <param name="dataMembers">The attribute's data members.</param>
        /// <returns>The identifier of the created attribute.</returns>
        public static Guid CreateAttribute(
            Guid objectId, 
            string key, 
            string value = null,
            string xml = null, 
            Guid? refValue = null,
            params KeyValuePair<string, object>[] dataMembers)
        {
            Guid attributeId = Guid.NewGuid();
            int keyId = GetStringId(key);

            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@AttributeId", attributeId));
            parameters.Add(new SqlParameter("@ObjectId", objectId));
            parameters.Add(new SqlParameter("@AttributeKey", keyId));

            if (value != null)
            {
                parameters.Add(new SqlParameter("@TextValue", value));
            }
            else
            {
                parameters.Add(new SqlParameter("@TextValue", DBNull.Value));
            }

            if (xml != null)
            {
                parameters.Add(new SqlParameter("@XmlValue", xml));
            }
            else
            {
                parameters.Add(new SqlParameter("@XmlValue", DBNull.Value));
            }

            if (refValue != null)
            {
                parameters.Add(new SqlParameter("@GuidRefValue", refValue));
            }
            else
            {
                parameters.Add(new SqlParameter("@GuidRefValue", DBNull.Value));
            }

            ExecuteQuery(
                "INSERT INTO dbo.Attributes (AttributeId,ObjectId,AttributeKey,TextValue,XmlValue,GuidRefValue) " +
                "VALUES (@AttributeId,@ObjectId,@AttributeKey,@TextValue,@XmlValue,@GuidRefValue);",
                parameters.ToArray());

            foreach (KeyValuePair<string, object> dataMember in dataMembers)
            {
                ExecuteQuery(
                    "UPDATE dbo.Attributes SET " + dataMember.Key + " = @Value WHERE AttributeId = @AttributeId",
                    new SqlParameter("@Value", dataMember.Value),
                    new SqlParameter("@AttributeId", attributeId));
            }

            return attributeId;
        }

        /// <summary>
        /// Gets the data of the specified attribute.
        /// </summary>
        /// <param name="objectId">The identifier of the object on which the attribute is defined.</param>
        /// <param name="key">The key (name) of the attribute to get.</param>
        /// <returns>An array of field values of the specified attribute.</returns>
        public static object[] GetAttribute(Guid objectId, string key)
        {
            return GetTableRow(
                "AttributeId,ObjectId,AttributeKey,TextValue,XmlValue,GuidRefValue,StringProperty,Int,BooleanProperty",
                "dbo.AttributesView",
                "ObjectId = @ObjectId and AttributeKey = @AttributeKey",
                new SqlParameter("@ObjectId", objectId),
                new SqlParameter("@AttributeKey", key));
        }

        /// <summary>
        /// Gets the data of the attribute with the specified identifier.
        /// </summary>
        /// <param name="attributeId">The identifier of the attribute to get.</param>
        /// <returns>An array of field values of the specified attribute.</returns>
        public static object[] GetAttribute(Guid attributeId)
        {
            return GetTableRow(
                "AttributeId,ObjectId,AttributeKey,TextValue,XmlValue,GuidRefValue,StringProperty,Int,BooleanProperty",
                "dbo.AttributesView",
                "AttributeId = @AttributeId",
                new SqlParameter("@AttributeId", attributeId));
        }
    }
}