﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReflectionAttributeMemberManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements mapping attribute member properties to attribute store values using .NET reflection.
    /// </summary>
    public class ReflectionAttributeMemberManager : IAttributeMemberManager
    {
        /// <summary>
        /// Caches discovered attribute member mappings for attribute types.
        /// </summary>
        private static readonly IDictionary<Type, AttributeMemberMapping[]> Mappings = 
            new Dictionary<Type, AttributeMemberMapping[]>();

        /// <summary>
        /// Gets all attribute member mappings for the specified attribute type.
        /// </summary>
        /// <param name="attributeType">The attribute type to get mappings for.</param>
        /// <returns>A sequence of <see cref="IEnumerable{AttributeMemeberMapping}"/> objects.</returns>
        public IEnumerable<AttributeMemberMapping> GetMembersForAttributeType(Type attributeType)
        {
            if (attributeType == null)
            {
                throw new ArgumentNullException("attributeType");
            }

            AttributeMemberMapping[] mappings;
            
            lock (Mappings)
            {
                if (Mappings.TryGetValue(attributeType, out mappings))
                {
                    return mappings;
                }

                mappings = this.DiscoverMappings(attributeType);
                Mappings.Add(attributeType, mappings);
            }

            return mappings;
        }

        /// <summary>
        /// Discovers attribute member mappings for the specified attribute type.
        /// </summary>
        /// <param name="attributeType">The attribute type.</param>
        /// <returns>
        /// An array of <see cref="AttributeMemberMapping"/> objects which describe the mapped attribute member
        /// properties of the specified type.
        /// </returns>
        protected virtual AttributeMemberMapping[] DiscoverMappings(Type attributeType)
        {
            if (attributeType == null)
            {
                throw new ArgumentNullException("attributeType");
            }

            var mappings = new List<AttributeMemberMapping>();

            PropertyInfo[] properties = attributeType.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (PropertyInfo propertyInfo in properties)
            {
                AttributeMemberAttribute attribute = (AttributeMemberAttribute)Attribute.GetCustomAttribute(
                    propertyInfo,
                    typeof(AttributeMemberAttribute));

                if (attribute != null)
                {
                    string memberName = attribute.Name;
                    if (string.IsNullOrEmpty(memberName))
                    {
                        memberName = propertyInfo.Name;
                    }

                    mappings.Add(new AttributeMemberMapping(propertyInfo, memberName, attribute.IsReadOnly));
                }
            }

            return mappings.ToArray();
        }
    }
}