﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeMemberMapping.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Represents a mapping between an object attribute property and a value in the attribute store.
    /// </summary>
    public class AttributeMemberMapping
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeMemberMapping" /> class.
        /// </summary>
        /// <param name="property">The <see cref="PropertyInfo"/> object of the mapped property.</param>
        /// <param name="memberName">The name of the attribute member in the attribute store.</param>
        /// <param name="readOnly">
        /// <c>true</c> if the attribute member is read-only and should not be saved or updated in the attribute
        /// store; otherwise, <c>false</c>.
        /// </param>
        public AttributeMemberMapping(PropertyInfo property, string memberName, bool readOnly)
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }

            if (string.IsNullOrEmpty(memberName))
            {
                throw new ArgumentNullException("memberName");
            }

            this.Property = property;
            this.MemberName = memberName;
            this.IsReadOnly = readOnly;
        }

        /// <summary>
        /// Gets the <see cref="PropertyInfo"/> object of the mapped property.
        /// </summary>
        public PropertyInfo Property { get; private set; }

        /// <summary>
        /// Gets the name of the attribute member in the attribute store.
        /// </summary>
        public string MemberName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the attribute member should not be saved or updated in the attribute store.
        /// </summary>
        public bool IsReadOnly { get; private set; }
    }
}