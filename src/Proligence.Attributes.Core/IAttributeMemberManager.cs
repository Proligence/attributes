// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAttributeMemberManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the API for classes which manage mapping attribute member properties to attribute store values.
    /// </summary>
    public interface IAttributeMemberManager
    {
        /// <summary>
        /// Gets all attribute member mappings for the specified attribute type.
        /// </summary>
        /// <param name="attributeType">The attribute type to get mappings for.</param>
        /// <returns>A sequence of <see cref="IEnumerable{AttributeMemeberMapping}"/> objects.</returns>
        IEnumerable<AttributeMemberMapping> GetMembersForAttributeType(Type attributeType);
    }
}