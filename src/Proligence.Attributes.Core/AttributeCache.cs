﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeCache.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Caching;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Provides a standard implementation of the <see cref="IAttributeCache{TAttribute}"/> interface.
    /// </summary>
    /// <typeparam name="TAttribute">The type which represents the cached object attributes.</typeparam>
    public class AttributeCache<TAttribute> : IAttributeCache<TAttribute>
        where TAttribute : ObjectAttribute
    {
        /// <summary>
        /// The default maximum number of cache entries which can be stored in the cache.
        /// </summary>
        internal const int DefaultMaximumSize = 16384;

        /// <summary>
        /// The default number of minutes for the sliding expiration time span for cache entries.
        /// </summary>
        private const int DefaultSlidingExpiration = 20;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeCache&lt;TAttribute&gt;"/> class.
        /// </summary>
        /// <param name="objectCache">The underlying <see cref="ObjectCache"/>.</param>
        /// <param name="maximumSize">The maximum number of cache entries which can be stored in the cache.</param>
        public AttributeCache(ObjectCache objectCache, int maximumSize = DefaultMaximumSize)
            : this(objectCache, null, maximumSize)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeCache&lt;TAttribute&gt;"/> class.
        /// </summary>
        /// <param name="objectCache">The underlying <see cref="ObjectCache"/>.</param>
        /// <param name="cachePolicy">
        /// The <see cref="CacheItemPolicy"/> which will be used when adding new entries to the underlying
        /// object cache.
        /// </param>
        /// <param name="maximumSize">The maximum number of cache entries which can be stored in the cache.</param>
        public AttributeCache(
            ObjectCache objectCache,
            CacheItemPolicy cachePolicy,
            int maximumSize = DefaultMaximumSize)
        {
            if (objectCache == null)
            {
                throw new ArgumentNullException("objectCache");
            }

            this.ObjectCache = objectCache;
            this.CachePolicy = cachePolicy;
            this.MaximumSize = maximumSize;

            if (this.CachePolicy == null)
            {
                this.CachePolicy = new CacheItemPolicy
                {
                    SlidingExpiration = TimeSpan.FromMinutes(DefaultSlidingExpiration)
                };
            }
        }

        /// <summary>
        /// Gets the underlying <see cref="ObjectCache"/>.
        /// </summary>
        public ObjectCache ObjectCache { get; private set; }

        /// <summary>
        /// Gets the <see cref="CacheItemPolicy"/> which will be used when adding new entries to the underlying
        /// object cache.
        /// </summary>
        public CacheItemPolicy CachePolicy { get; private set; }

        /// <summary>
        /// Gets the maximum number of cache entries which can be stored in the cache.
        /// </summary>
        public int MaximumSize { get; private set; }

        /// <summary>
        /// Gets the cached attributes of the specified object or invokes the specified delegate and caches its result.
        /// </summary>
        /// <param name="objectId">The identifier of the object for which attributes will be returned.</param>
        /// <param name="func">The delegate which gets the object's attributes in case they are not cached.</param>
        /// <returns>The attributes of the specified object.</returns>
        public IEnumerable<TAttribute> AddOrGetAttributes(Guid objectId, Func<IEnumerable<TAttribute>> func)
        {
            if (func == null)
            {
                throw new ArgumentNullException("func");
            }

            IEnumerable<TAttribute> attributes = this.ObjectCache.Get(objectId.ToString()) as IEnumerable<TAttribute>;
            if (attributes == null)
            {
                attributes = func();
                this.ObjectCache.Set(objectId.ToString(), attributes, this.CachePolicy);
                this.EnsureAllowedCacheSize();
            }

            return attributes;
        }

        /// <summary>
        /// Adds the specified attribute to the cache.
        /// </summary>
        /// <param name="objectId">
        /// The identifier of the object for which the attributes will be added to the cache.
        /// </param>
        /// <param name="attributes">The attributes which will be added to the cache.</param>
        public void AddAttributes(Guid objectId, IEnumerable<TAttribute> attributes)
        {
            if (attributes == null)
            {
                throw new ArgumentNullException("attributes");
            }

            this.ObjectCache.Remove(objectId.ToString());
            this.ObjectCache.Add(objectId.ToString(), attributes, this.CachePolicy);
            this.EnsureAllowedCacheSize();
        }

        /// <summary>
        /// Removes all cached attributes of the specified object.
        /// </summary>
        /// <param name="objectId">
        /// The identifier of the object which attributes will be removed from the cache.
        /// </param>
        public void RemoveAttributes(Guid objectId)
        {
            this.ObjectCache.Remove(objectId.ToString());
        }

        /// <summary>
        /// Removes the oldest cache entries if the cache size is greater than the maximum allowed size.
        /// </summary>
        /// <remarks>
        /// This implementation is not exactly right, but let it be done this way for now. I can't find a better
        /// way to do this with a generic ObjectCache. We just remove the excess (and some more) amount of entries
        /// returned by the iterator, however, we should remove the oldest ones... Can't find a way to do that
        /// efficiently.
        /// <c>http://stackoverflow.com/questions/8043381/how-do-i-clear-a-system-runtime-caching-memorycache</c>
        /// </remarks>
        protected virtual void EnsureAllowedCacheSize()
        {
            long count = this.ObjectCache.GetCount();
            long trimCount = count - this.MaximumSize;
            
            if (trimCount > 0)
            {
                // Let's remove some more (10%) items so that we don't end up cleaning the cache each time a new
                // cache entry is added...
                trimCount += (int)(0.1 * count);

                var keysToRemove = new List<string>();
                foreach (KeyValuePair<string, object> entry in this.ObjectCache)
                {
                    if (trimCount <= 0)
                    {
                        break;
                    }

                    keysToRemove.Add(entry.Key);
                    trimCount--;
                }

                foreach (string key in keysToRemove)
                {
                    this.ObjectCache.Remove(key);
                }
            }
        }
    }
}