﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeManager.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Xml;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements reading and saving attributes from an <see cref="IAttributeStore"/>.
    /// </summary>
    /// <typeparam name="TAttribute">The type which represents the managed object attributes.</typeparam>
    public class AttributeManager<TAttribute> : IAttributeManager<TAttribute>
        where TAttribute : ObjectAttribute, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeManager{TAttribute}"/> class.
        /// </summary>
        /// <param name="store">The underlying attribute store.</param>
        /// <param name="cache">The attribute cache instance.</param>
        /// <param name="memberManager">The attribute member manager instance.</param>
        public AttributeManager(
            IAttributeStore store,
            IAttributeCache<TAttribute> cache,
            IAttributeMemberManager memberManager)
        {
            if (store == null)
            {
                throw new ArgumentNullException("store");
            }

            if (cache == null)
            {
                throw new ArgumentNullException("cache");
            }

            this.Store = store;
            this.Cache = cache;
            this.MemberManager = memberManager;
        }

        /// <summary>
        /// Gets the underlying attribute store.
        /// </summary>
        protected IAttributeStore Store { get; private set; }

        /// <summary>
        /// Gets the attribute cache instance used by the attribute manager.
        /// </summary>
        protected IAttributeCache<TAttribute> Cache { get; private set; }

        /// <summary>
        /// Gets the attribute member manager instance used by the attribute manager.
        /// </summary>
        protected IAttributeMemberManager MemberManager { get; private set; }

        /// <summary>
        /// Gets all attributes of the specified object.
        /// </summary>
        /// <param name="objectId">The identifier of the object which attributes will be returned.</param>
        /// <returns>A sequence of attributes associated with the specified object.</returns>
        public IEnumerable<TAttribute> GetObjectAttributes(Guid objectId)
        {
            return this.Cache.AddOrGetAttributes(
                objectId, 
                () => this.Store.GetAttributes(objectId).Select(this.ToAttribute));
        }

        /// <summary>
        /// Gets all attributes with the specified key of the specified object.
        /// </summary>
        /// <param name="objectId">The identifier of the object which attributes will be returned.</param>
        /// <param name="key">The key (name) of the attributes to get.</param>
        /// <returns>A sequence of attributes associated with the specified object.</returns>
        public IEnumerable<TAttribute> GetObjectAttributes(Guid objectId, string key)
        {
            IEnumerable<TAttribute> objectAttributes = this.Cache.AddOrGetAttributes(
                objectId,
                () => this.Store.GetAttributes(objectId).Select(this.ToAttribute));

            return objectAttributes.Where(attr => attr.Key == key);
        }

        /// <summary>
        /// Gets all attributes of the specified object which satisfy the specified criteria.
        /// </summary>
        /// <param name="objectId">The identifier of the object which attributes will be returned.</param>
        /// <param name="selector">The delegate which filters the attributes to get.</param>
        /// <returns>A sequence of attributes associated with the specified object.</returns>
        public IEnumerable<TAttribute> SelectObjectAttributes(Guid objectId, Func<TAttribute, bool> selector)
        {
            if (selector == null)
            {
                throw new ArgumentNullException("selector");
            }

            IEnumerable<TAttribute> objectAttributes = this.Cache.AddOrGetAttributes(
                objectId,
                () => this.Store.GetAttributes(objectId).Select(this.ToAttribute));

            return objectAttributes.Where(selector);
        }

        /// <summary>
        /// Gets the attribute with the specified identifier.
        /// </summary>
        /// <param name="attributeId">The identifier of the attribute to get.</param>
        /// <returns>
        /// The object which represents the specified attribute or <c>null</c> if the specified attribute was not
        /// found.
        /// </returns>
        public TAttribute GetAttribute(Guid attributeId)
        {
            AttributeData attributeData = this.Store.GetAttribute(attributeId);
            if (attributeData != null)
            {
                return this.ToAttribute(attributeData);
            }

            return null;
        }

        /// <summary>
        /// Saves the specified attributes in the store.
        /// </summary>
        /// <param name="attributes">The sequence of attributes to save in the store.</param>
        /// <param name="overwriteExisting"><c>true</c> to overwrite existing attributes with the same key.</param>
        public void SaveAttributes(IEnumerable<TAttribute> attributes, bool overwriteExisting)
        {
            if (attributes == null)
            {
                throw new ArgumentNullException("attributes");
            }

            // Make sure that we do not enumerate the specified IEnumerable multiple times!
            attributes = attributes.ToArray();

            if (attributes.Any(attr => attr == null))
            {
                throw new ArgumentException(
                    "At least one of the specified attributes is null.",
                    "attributes");
            }

            if (attributes.Any(attr => string.IsNullOrEmpty(attr.Key)))
            {
                throw new ArgumentException(
                    "At least one of the specified attributes has an invalid key (name).",
                    "attributes");
            }

            AttributeData[] attributesData = attributes.Select(this.ToAttributeData).ToArray();
            this.Store.SaveAttributes(attributesData, overwriteExisting);

            int index = 0;
            foreach (TAttribute attribute in attributes)
            {
                attribute.AttributeId = attributesData[index++].AttributeId;
            }

            IEnumerable<Guid> objectIds = attributes.Select(x => x.ObjectId).Distinct();
            foreach (Guid objectId in objectIds)
            {
                this.Cache.RemoveAttributes(objectId);
            }
        }

        /// <summary>
        /// Removes attributes with the specified identifiers.
        /// </summary>
        /// <param name="attributeIds">The identifiers of the attributes to remove.</param>
        public void RemoveAttributes(IEnumerable<Guid> attributeIds)
        {
            if (attributeIds == null)
            {
                throw new ArgumentNullException("attributeIds");
            }

            attributeIds = attributeIds.ToArray();

            IEnumerable<Guid> objectIds = attributeIds
                .Select(this.GetAttribute)
                .Where(attr => attr != null)
                .Select(attr => attr.ObjectId)
                .Distinct()
                .ToArray();

            this.Store.RemoveAttributes(attributeIds);
            
            foreach (Guid objectId in objectIds)
            {
                this.Cache.RemoveAttributes(objectId);
            }
        }

        /// <summary>
        /// Gets the identifiers of all objects which are associated with the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the associated attributes or <c>null</c>.</param>
        /// <param name="textValue">The text value of the associated attributes or <c>null</c>.</param>
        /// <param name="refValue">The referenced object value of the associated attributes or <c>null</c>.</param>
        /// <returns>
        /// A sequence of distinct identifiers of the objects that are associated with at least one attribute which
        /// fulfills the specified attribute conditions.
        /// </returns>
        public IEnumerable<Guid> GetObjects(string key, string textValue = null, Guid? refValue = null)
        {
            return this.Store.GetObjects(key, textValue, refValue);
        }

        /// <summary>
        /// Gets the identifiers of all objects which are associated with the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the associated attributes or <c>null</c>.</param>
        /// <param name="textValues">
        /// The set of text values of the associated attributes or <c>null</c>. If the attribute matches any value
        /// from the list, the associated object identifier will be returned.
        /// </param>
        /// <param name="refValue">The referenced object value of the associated attributes or <c>null</c>.</param>
        /// <returns>
        /// A sequence of distinct identifiers of the objects that are associated with at least one attribute which
        /// fulfills the specified attribute conditions.
        /// </returns>
        public IEnumerable<Guid> GetObjectsBySet(string key, IEnumerable<string> textValues, Guid? refValue = null)
        {
            var setQueryable = this.Store as ISetQueryableStore;
            if (setQueryable != null)
            {
                return setQueryable.GetObjectsBySet(key, textValues, refValue);
            }

            if (textValues != null)
            {
                var results = new SortedSet<Guid>();
                foreach (string textValue in textValues)
                {
                    foreach (Guid objectId in this.GetObjects(key, textValue, refValue))
                    {
                        results.Add(objectId);
                    }
                }

                return results;
            }

            return this.GetObjects(key, null, refValue);
        }

        /// <summary>
        /// Gets the identifiers of all objects referenced by the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the attributes or <c>null</c>.</param>
        /// <param name="textValue">The text value of the associated attributes or <c>null</c>.</param>
        /// <param name="objectId">The identifier of the object associated with the attributes or <c>null</c>.</param>
        /// <returns>A sequence of distinct identifiers of objects referenced from the specified attributes.</returns>
        public IEnumerable<Guid> GetReferencedObjects(string key, string textValue = null, Guid? objectId = null)
        {
            return this.Store.GetReferencedObjects(key, textValue, objectId);
        }

        /// <summary>
        /// Gets the identifiers of all objects referenced by the specified attributes.
        /// </summary>
        /// <param name="key">The key (name) of the attributes or <c>null</c>.</param>
        /// <param name="textValues">
        /// The set of text values of the associated attributes or <c>null</c>. If the attribute matches any value
        /// from the list, the associated object identifier will be returned.
        /// </param>
        /// <param name="objectId">The identifier of the object associated with the attributes or <c>null</c>.</param>
        /// <returns>A sequence of distinct identifiers of objects referenced from the specified attributes.</returns>
        public IEnumerable<Guid> GetReferencedObjectsBySet(
            string key,
            IEnumerable<string> textValues,
            Guid? objectId = null)
        {
            var setQueryable = this.Store as ISetQueryableStore;
            if (setQueryable != null)
            {
                return setQueryable.GetReferencedObjectsBySet(key, textValues, objectId);
            }

            if (textValues != null)
            {
                var results = new SortedSet<Guid>();
                foreach (string textValue in textValues)
                {
                    foreach (Guid refId in this.GetReferencedObjects(key, textValue, objectId))
                    {
                        results.Add(refId);
                    }
                }

                return results;
            }

            return this.GetObjects(key, null, objectId);
        }

        /// <summary>
        /// Converts the specified <see cref="AttributeData"/> to a <see cref="TAttribute"/> object.
        /// </summary>
        /// <param name="attributeData">The attribute data.</param>
        /// <returns>The created <see cref="TAttribute"/> object.</returns>
        protected virtual TAttribute ToAttribute(AttributeData attributeData)
        {
            if (attributeData == null)
            {
                throw new ArgumentNullException("attributeData");
            }

            var attribute = new TAttribute
            {
                AttributeId = attributeData.AttributeId,
                ObjectId = attributeData.ObjectId,
                Key = attributeData.Key,
                TextValue = attributeData.TextValue,
                GuidRefValue = attributeData.GuidRefValue
            };

            AttributeMemberMapping[] memberMappings =
                this.MemberManager.GetMembersForAttributeType(typeof(TAttribute)).ToArray();

            KeyValuePair<string, object>[] dataMembers = attributeData.DataMembers.ToArray();

            foreach (KeyValuePair<string, object> attributeMember in dataMembers)
            {
                AttributeMemberMapping mapping = memberMappings.SingleOrDefault(
                    x => x.MemberName == attributeMember.Key);

                if (mapping != null)
                {
                    mapping.Property.SetValue(attribute, attributeMember.Value, new object[0]);
                }
            }

            if (attributeData.XmlValue != null)
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(attributeData.XmlValue);
                attribute.XmlValue = xmlDocument;
            }

            return attribute;
        }

        /// <summary>
        /// Converts the specified <see cref="TAttribute"/> to an <see cref="AttributeData"/> object.
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        /// <returns>The created <see cref="AttributeData"/> object.</returns>
        protected virtual AttributeData ToAttributeData(TAttribute attribute)
        {
            var attributeData = new AttributeData
            {
                AttributeId = attribute.AttributeId,
                ObjectId = attribute.ObjectId,
                Key = attribute.Key,
                TextValue = attribute.TextValue,
                GuidRefValue = attribute.GuidRefValue
            };

            if (attribute.XmlValue != null)
            {
                attributeData.XmlValue = attribute.XmlValue.CreateNavigator().OuterXml;
            }

            IEnumerable<AttributeMemberMapping> memberMappings =
                this.MemberManager.GetMembersForAttributeType(typeof(TAttribute));

            foreach (AttributeMemberMapping memberMapping in memberMappings)
            {
                if (memberMapping.IsReadOnly)
                {
                    continue;
                }

                object value = memberMapping.Property.GetValue(attribute, new object[0]);
                attributeData.DataMembers.Add(memberMapping.MemberName, value);
            }

            return attributeData;
        }
    }

    /// <summary>
    /// Implements the <see cref="IAttributeManager{TAttribute}"/> manager for <see cref="ObjectAttribute"/>
    /// attributes.
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass",
        Justification = "By design.")]
    public class AttributeManager : AttributeManager<ObjectAttribute>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeManager"/> class.
        /// </summary>
        /// <param name="store">The underlying attribute store.</param>
        /// <param name="cache">The attribute cache instance.</param>
        /// <param name="memberManager">The attribute member manager.</param>
        public AttributeManager(
            IAttributeStore store,
            IAttributeCache<ObjectAttribute> cache,
            IAttributeMemberManager memberManager)
            : base(store, cache, memberManager)
        {
        }
    }
}