﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttributeManagerExtensions.cs" company="Proligence">
//   Copyright (C) Proligence
// </copyright>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
// For commercial license contact info@proligence.pl.
// --------------------------------------------------------------------------------------------------------------------

namespace Proligence.Attributes.Core
{
    using System;
    using System.Linq;
    using Proligence.Attributes.Common;

    /// <summary>
    /// Implements extension methods for the <see cref="AttributeManager{TAttribute}"/> class.
    /// </summary>
    public static class AttributeManagerExtensions
    {
        /// <summary>
        /// Gets a single attribute with the specified key which is associated with the specified object.
        /// </summary>
        /// <typeparam name="TAttribute">The type which represents the attribute to get.</typeparam>
        /// <param name="attributeManager">The attribute manager instance.</param>
        /// <param name="objectId">The identifier of the object which the attribute belongs to.</param>
        /// <param name="key">The key (name) of the attribute to get.</param>
        /// <returns>The specified attribute or <c>null</c> if no attribute with the specified key was found.</returns>
        public static TAttribute GetAttribute<TAttribute>(
            this IAttributeManager<TAttribute> attributeManager,
            Guid objectId,
            string key)
            where TAttribute : ObjectAttribute
        {
            if (attributeManager == null)
            {
                throw new ArgumentNullException("attributeManager");
            }

            return attributeManager.GetObjectAttributes(objectId, key).FirstOrDefault();
        }

        /// <summary>
        /// Saves a single attribute.
        /// </summary>
        /// <typeparam name="TAttribute">The type which represents the attribute to save.</typeparam>
        /// <param name="attributeManager">The attribute manager instance.</param>
        /// <param name="attribute">The attribute to save.</param>
        public static void SaveAttribute<TAttribute>(
            this IAttributeManager<TAttribute> attributeManager,
            TAttribute attribute)
            where TAttribute : ObjectAttribute
        {
            if (attributeManager == null)
            {
                throw new ArgumentNullException("attributeManager");
            }

            if (attribute == null)
            {
                throw new ArgumentNullException("attribute");
            }

            attributeManager.SaveAttributes(new[] { attribute });
        }

        /// <summary>
        /// Removes a single attribute.
        /// </summary>
        /// <typeparam name="TAttribute">The type which represents the attribute to remove.</typeparam>
        /// <param name="attributeManager">The attribute manager instance.</param>
        /// <param name="attributeId">The identifier of the attribute to remove.</param>
        public static void RemoveAttribute<TAttribute>(
            this IAttributeManager<TAttribute> attributeManager,
            Guid attributeId)
            where TAttribute : ObjectAttribute
        {
            if (attributeManager == null)
            {
                throw new ArgumentNullException("attributeManager");
            }

            attributeManager.RemoveAttributes(new[] { attributeId });
        }
    }
}